#!/bin/bash
echo "Installing ipaaca-cpp"

declare -a CMAKE_PLATFORM_FLAGS

INCLUDE_PATH="${PREFIX}/include"
LIBRARY_PATH="${PREFIX}/lib"

export CMAKE_INCLUDE_PATH=$INCLUDE_PATH
export CMAKE_LIBRARY_PATH=$LIBRARY_PATH

if [[ "$(uname)" == "Linux" ]]; then
    # Linus needs this lrt flag
    export LDFLAGS="$LDFLAGS -lrt"
    CMAKE_PLATFORM_FLAGS+=(-DCMAKE_TOOLCHAIN_FILE="${RECIPE_DIR}/cross-linux.cmake")
elif [[ "$(uname)" == "Darwin" ]]; then
    echo "mac"
fi

# build protobuf files
mkdir -p ipaaca-cpp/build/ipaaca
cd proto
protoc ipaaca.proto --cpp_out=../ipaaca-cpp/build/ipaaca

# build ipaaca cpp
cd ../ipaaca-cpp/build
export VERBOSE=1

cmake .. -DCMAKE_INSTALL_PREFIX=${PREFIX} -DBOOST_INCLUDEDIR=${INCLUDE_PATH} -DBOOST_LIBRARYDIR=${LIBRARY_PATH} ${CMAKE_PLATFORM_FLAGS[@]}
echo "Finished cmake, trying build now"
make 
make install DESTDIR=${PREFIX}
cd ../../..