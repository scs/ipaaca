#!/bin/bash

# Since we setup subfolders for ipaaca and proto, we want to step into the ipaaca folder here
# Note: setup.py assumes protoc file to be at ../proto from here and we do not want to change
# that for easier manual installation
cd ipaaca-py
echo "Installing ipaaca using setup.py"
$PYTHON -m pip install . -vv --no-deps
#python setup.py install --single-version-externally-managed --record=record.txt
