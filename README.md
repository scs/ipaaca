# IPAACA

This repository contains the software library IPAACA developed by the Social Cognitive Systems Group at Bielefeld University. IPAACA stands for 'Incremental Processing Architecture for Artificial Conversational Agents.' The library is available in three languages: Python (Python3 compatible), C++, and Java, and for three operating systems: Linux, OS X, and Windows.

## Build dependencies 

### Linux
Dependencies: Protocol Buffer (libprotobuf), Transport Protocol (**libmosquittopp**, ros, librsb)

sudo apt-get install libprotobuf-dev

sudo apt-get install libprotoc-dev

sudo apt-get install protobuf-compiler

sudo apt-get install mosquitto libmosquittopp-dev

## Build/Install instructions 

### Linux
**Compiling C++ Version** 

cd ipaaca/ipaacalib/cpp

mkdir build

cd build

cmake ..

make

sudo make install

**Installing Python Version**

cd ipaaca/ipaacalib/python

python3 setup.py install --user

### OS X

### Windows

## Usage

**Python:** import ipaaca

**C++:** #include "ipaaca/ipaaca.h"

## History


## Credits
Hendrik Buschmeier <hbuschme@techfak.uni-bielefeld.de> 

Ramin Yaghoubzadeh <ryaghoub@techfak.uni-bielefeld.de>

## License
GNU LESSER GENERAL PUBLIC LICENSE (See LICENSE.txt in the repository)

## Further Reading
[1] IPAACA: https://scs.techfak.uni-bielefeld.de/wiki/public/ipaaca/start

[2] Schlangen et al. "Middleware for Incremental Processing in Conversational Agents," SIGDIAL 2010. https://www.aclweb.org/anthology/W10-4308
