Instructions for building ipaaca on Windows

Install Visual Studio 2017 (with C++), including CMake
Install GoW (Gnu on Windows) to have some POSIX-like utilities, or double-check the commands below

Open terminal via Visual Studio's "x64 Native Tools Command Prompt", for 64-bit builds

The following instructions are for Debug mode, you can use Release, too
(replace all occurrences below).


BUILD DEPS:

protobuf (latest test with 3.9.0):

cd cmake
mkdir build
cd build
cmake -Dprotobuf_BUILD_TESTS=OFF -DCMAKE_INSTALL_PREFIX="C:/Libs/protobuf/x64" -G "Visual Studio 15 2017 Win64" /p:Configuration=Debug ..
cmake --build . --target INSTALL --config Debug -- /nologo /verbosity:minimal /maxcpucount

mosquitto (latest test with 1.5.4):
mkdir build
cd build
# threading=off below disables a nasty library dependency, our dispatcher is threaded anyway
cmake -DWITH_THREADING=OFF -DCMAKE_INSTALL_PREFIX=C:/Libs/mosquitto/x64 -G "Visual Studio 15 2017 Win64" /p:Configuration=Debug ..
cmake --build . --target INSTALL --config Debug -- /nologo /verbosity:minimal /maxcpucount
cp lib\Debug\*.lib lib\cpp\Debug\*.lib \Libs\mosquitto\x64\


BUILD IPAACA (ipaaca4):

cd ......\repo\ipaaca\ipaacalib\proto
mkdir ..\cpp\build\ipaaca
\Libs\protobuf\x64\bin\protoc.exe ipaaca.proto --cpp_out=..\cpp\build\ipaaca
cd ..\cpp\build\ipaaca
cmake -DCMAKE_INSTALL_PREFIX="C:/Libs/ipaaca/x64" -G "Visual Studio 15 2017 Win64" /p:Configuration=Debug ..
cmake --build . --target INSTALL --config Debug -- /nologo /verbosity:minimal /maxcpucount


SETUP FOR VISUAL STUDIO PROJECTS:
Project -> Properties ->
Debugging: add the runtime paths to the environment variables (Path=%Path%;C:\Libs\ipaaca\x64\bin;C:\Libs\mosquitto\x64\bin)
C++: add the include directories (C:\Libs\ipaaca\x64\include;C:\Libs\protobuf\x64\include)
Linker: add ipaaca.lib to the dependencies and C:\Libs\ipaaca\x64\lib to the additional lib dirs
Also add /wd4146 to compiler options if you get errors in protobuf headers (like "unary negative on unsigned value").


