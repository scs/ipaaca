/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

#include <ipaaca/ipaaca.h>

namespace ipaaca {

// static library Initializer
IPAACA_EXPORT bool Initializer::_initialized = false;
IPAACA_EXPORT bool Initializer::initialized() { return _initialized; }
IPAACA_EXPORT void Initializer::initialize_ipaaca_rsb_if_needed()
{
	initialize_backend();
}
IPAACA_EXPORT void Initializer::initialize_backend()//{{{
{
	if (_initialized) return;

	override_env_with_cmdline_set_variables();

	_initialized = true;
}//}}}
IPAACA_EXPORT void Initializer::dump_current_default_config()//{{{
{
    IPAACA_INFO("--- Dumping current global configuration ---")
    auto cfg = ipaaca::get_global_config();
    for (auto it = cfg->data_cbegin(); it != cfg->data_cend(); ++it) {
        IPAACA_INFO("--- " << it->first << " = \"" << it->second << "\"")
    }
    IPAACA_INFO("-------------- End of config ---------------")
}//}}}
IPAACA_EXPORT void Initializer::override_env_with_cmdline_set_variables()//{{{
{
	// set RSB host and port iff provided using cmdline arguments
	if (__ipaaca_static_option_rsb_host!="") {
		IPAACA_INFO("Overriding RSB host with " << __ipaaca_static_option_rsb_host)
		IPAACA_SETENV("RSB_TRANSPORT_SPREAD_HOST", __ipaaca_static_option_rsb_host.c_str())
		IPAACA_SETENV("RSB_TRANSPORT_SOCKET_HOST", __ipaaca_static_option_rsb_host.c_str());
	}
	if (__ipaaca_static_option_rsb_port!="") {
		IPAACA_INFO("Overriding RSB port with " << __ipaaca_static_option_rsb_port)
		IPAACA_SETENV("RSB_TRANSPORT_SPREAD_PORT", __ipaaca_static_option_rsb_port.c_str());
		IPAACA_SETENV("RSB_TRANSPORT_SOCKET_PORT", __ipaaca_static_option_rsb_port.c_str());
	}
	if (__ipaaca_static_option_rsb_transport!="") {
		if (__ipaaca_static_option_rsb_transport == "spread") {
			IPAACA_INFO("Overriding RSB transport mode - using 'spread' ")
			IPAACA_SETENV("RSB_TRANSPORT_SPREAD_ENABLED", "1");
			IPAACA_SETENV("RSB_TRANSPORT_SOCKET_ENABLED", "0");
		} else if (__ipaaca_static_option_rsb_transport == "socket") {
			IPAACA_INFO("Overriding RSB transport mode - using 'socket' ")
			IPAACA_SETENV("RSB_TRANSPORT_SPREAD_ENABLED", "0");
			IPAACA_SETENV("RSB_TRANSPORT_SOCKET_ENABLED", "1");
			if (__ipaaca_static_option_rsb_socketserver!="") {
				const std::string& srv = __ipaaca_static_option_rsb_socketserver;
				if ((srv=="1")||(srv=="0")||(srv=="auto")) {
					IPAACA_INFO("Overriding RSB transport.socket.server with " << srv)
					IPAACA_SETENV("RSB_TRANSPORT_SOCKET_SERVER", srv.c_str());
				} else {
					IPAACA_INFO("Unknown RSB transport.socket.server mode " << srv << " - using config default ")
				}
			}
		} else {
			IPAACA_INFO("Unknown RSB transport mode " <<  __ipaaca_static_option_rsb_transport << " - using config default ")
		}
	}
}//}}}

} // of namespace ipaaca
