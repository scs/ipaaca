/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca-backend-mqtt.cc
 *
 * \brief Source file for the MQTT backend
 *
 * \author Ramin Yaghoubzadeh Torky (ryaghoubzadeh@uni-bielefeld.de)
 * \date   January, 2019
 */

#include <ipaaca/ipaaca.h>

#include <thread>
#include <chrono>

namespace ipaaca {
#include <ipaaca/ipaaca-backend-mqtt.h>
namespace backend {
namespace mqtt {

// The following is a required static library initialization hook.
//   This way, the backend gets registered into the global store just by getting selectively
//   compiled in (i.e. without insertion into the general code or plugin loading).
//   The back-end name is taken from the one provided in the BackEnd constructor.
IPAACA_EXPORT static bool __initialize_mqtt_backend = BackEndLibrary::get()->register_backend(MQTTBackEnd::get());

// MQTTBackEnd{{{
// The backend class is the interface to the rest of the IPAACA library,
//  which does not know any of the implementation details here.
//  It is available via its (unique) given name (here "mqtt", just below)
IPAACA_EXPORT MQTTBackEnd::MQTTBackEnd()
: BackEnd("mqtt") {
}
IPAACA_EXPORT BackEnd::ptr MQTTBackEnd::get() {
    static ptr backend_singleton;
    if (!backend_singleton) {
        ::mosqpp::lib_init();
        backend_singleton = std::shared_ptr<MQTTBackEnd>(new MQTTBackEnd());
    }
    return backend_singleton;
}
IPAACA_EXPORT Informer::ptr MQTTBackEnd::createInformer(const std::string& scope)
{
    auto res = std::make_shared<MQTTInformer>(generate_client_id(), scope, get_global_config());
    res->wait_live();
    // TODO wait for it to come live?
    return res;
}
IPAACA_EXPORT Listener::ptr MQTTBackEnd::createListener(const std::string& scope, InputBuffer* buf)
{
    auto res = std::make_shared<MQTTListener>(generate_client_id(), scope, buf, get_global_config());
    res->wait_live();
    // TODO wait for it to come live?
    return res;
}
IPAACA_EXPORT LocalServer::ptr MQTTBackEnd::createLocalServer(const std::string& scope, OutputBuffer* buf)
{
    auto res = std::make_shared<MQTTLocalServer>(generate_client_id(), scope, buf, get_global_config());
    res->wait_live();
    // TODO wait for it to come live?
    return res;
}
IPAACA_EXPORT RemoteServer::ptr MQTTBackEnd::createRemoteServer(const std::string& scope)
{
    auto res = std::make_shared<MQTTRemoteServer>(generate_client_id(), scope, get_global_config());
    res->wait_live();
    //res->wait_live();
    // TODO wait for it to come live?
    return res;
}
IPAACA_EXPORT void MQTTBackEnd::teardown()
{
    IPAACA_DEBUG("MQTTBackEnd sleeping for 1 sec before teardown, for messages in transit.")
    std::this_thread::sleep_for(std::chrono::seconds(1));
}
//}}}

//
// Internal implementation follows
//

// ParticipantCore{{{
IPAACA_EXPORT ParticipantCore::ParticipantCore()
: _running(false), _live(false)
{
}
IPAACA_EXPORT void ParticipantCore::signal_live() {
    IPAACA_DEBUG("Notifying to wake up an async MQTT session (now live)")
    _live = true;
    _condvar.notify_one();
}
IPAACA_EXPORT bool ParticipantCore::wait_live(long timeout_milliseconds) {
    IPAACA_DEBUG("Waiting for an MQTT session to come live")
    std::unique_lock<std::mutex> lock(_condvar_mutex);
    // mqtt handlers will notify this after connect or subscribe (depending on the subclass)
    auto success = _condvar.wait_for(lock, std::chrono::milliseconds(timeout_milliseconds), [this]{return this->_live;});
    if (!success) {
        IPAACA_ERROR("Backend timeout: failed to go live")
        return false; // TODO throw here or in construction wrapper (below)
    }
    return true;
}
//}}}

// MQTTParticipant{{{
IPAACA_EXPORT int MQTTParticipant::get_next_mid() {
    static int _curmid = 0;
    _curmid++;
    return _curmid;
}

IPAACA_EXPORT MQTTParticipant::MQTTParticipant(const std::string& client_id, const std::string& scope, Config::ptr config)
: ParticipantCore(), ::mosqpp::mosquittopp(client_id.c_str(), true), _scope(scope)
{
    //threaded_set(true);
    _client_id = client_id;
    // get connection parameters from config
    if (config) {
        host = config->get_with_default_and_warning<std::string>("transport.mqtt.host", "localhost");
        std::cout << "HOST from config: " << host << std::endl;
        port = config->get_with_default_and_warning<int>("transport.mqtt.port", 1883);
        keepalive = config->get_with_default<int>("transport.mqtt.keepalive", 60);
    } else {
        host = "localhost";
        port = 1883;
        keepalive = 60;
        IPAACA_ERROR("No Config provided in MQTT backend, using defaults: host=localhost port=1883 keepalive=60")
    }
    IPAACA_DEBUG("Created MQTTParticipant on " << host << ":" << port << " for scope " << _scope << " with prepared client id " << _client_id)
}

IPAACA_EXPORT void MQTTParticipant::connect_and_background()
{
    static const char* mosquitto_reasons[] = {
        //"MOSQ_ERR_CONN_PENDING" is -1
        "MOSQ_ERR_SUCCESS",
        "MOSQ_ERR_NOMEM",
        "MOSQ_ERR_PROTOCOL",
        "MOSQ_ERR_INVAL",
        "MOSQ_ERR_NO_CONN",
        "MOSQ_ERR_CONN_REFUSED",
        "MOSQ_ERR_NOT_FOUND",
        "MOSQ_ERR_CONN_LOST",
        "MOSQ_ERR_TLS",
        "MOSQ_ERR_PAYLOAD_SIZE",
        "MOSQ_ERR_NOT_SUPPORTED",
        "MOSQ_ERR_AUTH",
        "MOSQ_ERR_ACL_DENIED",
        "MOSQ_ERR_UNKNOWN",
        "MOSQ_ERR_ERRNO",
        "MOSQ_ERR_EAI",
        "MOSQ_ERR_PROXY"
    };
    int res = connect(host.c_str(), port, keepalive);
    loop_start();
    if (res!=0) {
        const char* reason = "unknown";
        if (res==-1) {
            reason = "MOSQ_ERR_CONN_PENDING";
        } else if ((res>0) && (res<17)) {
            reason = mosquitto_reasons[res];
        }
        IPAACA_ERROR("MQTT connect (for scope " << _scope << ") returned an error " << res << " (mosquitto's " << reason << ") - please double-check parameters")
        if (res==14) {
            IPAACA_ERROR("The underlying system error: errno " << errno << " (" << strerror(errno) << ")")
        }
        throw BackEndConnectionFailedError();
    } else {
        IPAACA_DEBUG("connect OK for scope " << _scope)
    }
}
IPAACA_EXPORT void MQTTParticipant::on_error()
{
    IPAACA_ERROR("MQTT error")
}

IPAACA_EXPORT void MQTTParticipant::on_disconnect(int rc)
{
    IPAACA_ERROR("MQTT disconnect of " << _scope << " with rc " << rc)
}
//}}}

// MQTTInformer {{{
IPAACA_EXPORT MQTTInformer::MQTTInformer(const std::string& client_id, const std::string& scope, Config::ptr config)
: MQTTParticipant(client_id, scope, config)
{
    IPAACA_DEBUG("Create MQTTInformer for scope " << ((std::string) scope))
    connect_and_background();
}
IPAACA_EXPORT void MQTTInformer::on_connect(int rc)
{
    signal_live();
}
IPAACA_EXPORT bool MQTTInformer::internal_publish(const std::string& wire)
{
    IPAACA_DEBUG("Trying to publish via MQTT, topic " << _scope)
    //int mid = MQTTParticipant::get_next_mid();
    int result = mosquittopp::publish(NULL, _scope.c_str(), wire.size(), wire.c_str(), 2, false);
    return (result==0);
}
//}}}

// MQTTListener {{{
IPAACA_EXPORT MQTTListener::MQTTListener(const std::string& client_id, const std::string& scope, InputBuffer* buffer_ptr, Config::ptr config)
: MQTTParticipant(client_id, scope, config), Listener(buffer_ptr)
{
    IPAACA_DEBUG("Create MQTTListener for scope " << ((std::string) scope))
    connect_and_background();
}
IPAACA_EXPORT void MQTTListener::on_connect(int rc)
{
    int res = subscribe(NULL, _scope.c_str(), 2);
    if (res!=0) {
        IPAACA_ERROR("subscribe (on topic " << _scope << ") returned an error " << res)
    } else {
        IPAACA_DEBUG("subscribe returned OK for topic " << _scope)
    }
}

IPAACA_EXPORT void MQTTListener::on_subscribe(int mid, int qos_count, const int * granted_qos)
{
    //IPAACA_DEBUG("on_subscribe: " << mid << " " << qos_count << " for scope " << _scope)
    if (qos_count < 1) {
        IPAACA_WARNING("No QoS grants reported")
    } else if (qos_count > 1) {
        IPAACA_WARNING("More than one QoS grant reported for Listener, should not happen")
    } else {
        int qos = granted_qos[0];
        if (qos!=2) {
            IPAACA_WARNING("MQTT QoS level 2 (guaranteed delivery) has NOT been granted on " << _scope << " (we got level " << qos << ")")
        }
    }
    signal_live();
}

IPAACA_EXPORT void MQTTListener::on_message(const struct mosquitto_message * message) {
    // internal_deserialize expects a string, which we construct here from the received char* and len
    auto event = ipaaca::converters::internal_deserialize(std::string((const char*) message->payload, message->payloadlen));
    //std::cout << "GOT AN EVENT of type " << event->getType() << std::endl;
    // let the Listener base class handle the propagation into a Buffer:
    Listener::relay_received_event_to_buffer_threaded(event);
}
//}}}

// MQTTLocalServer {{{
IPAACA_EXPORT MQTTLocalServer::MQTTLocalServer(const std::string& client_id, const std::string& scope, ipaaca::OutputBuffer* buffer_ptr, Config::ptr config)
: MQTTParticipant(client_id, scope, config), LocalServer(buffer_ptr)
{
    IPAACA_DEBUG("Create MQTTLocalServer for scope " << ((std::string) scope));
    connect_and_background();
}
IPAACA_EXPORT void MQTTLocalServer::on_connect(int rc)
{
    //IPAACA_DEBUG("LocalServer::on_connect: " << rc)
    int res = subscribe(NULL, _scope.c_str(), 2);
    if (res!=0) {
        IPAACA_ERROR("subscribe (on topic " << _scope << ") returned an error " << res)
    } else {
        IPAACA_DEBUG("subscribe returned OK for topic " << _scope)
    }
}

IPAACA_EXPORT void MQTTLocalServer::on_subscribe(int mid, int qos_count, const int * granted_qos)
{
    //IPAACA_DEBUG("LocalServer::on_subscribe: " << mid << " " << qos_count << " for scope " << _scope)
    if (qos_count < 1) {
        IPAACA_WARNING("No QoS grants reported")
    } else if (qos_count > 1) {
        IPAACA_WARNING("More than one QoS grant reported for Listener, should not happen")
    } else {
        int qos = granted_qos[0];
        if (qos!=2) {
            IPAACA_WARNING("MQTT QoS level 2 (guaranteed delivery) has NOT been granted on " << _scope << " (we got level " << qos << ")")
        }
    }
    signal_live();
}
IPAACA_EXPORT void MQTTLocalServer::send_result_for_request(const std::string& request_endpoint, const std::string& request_uid, int64_t result)
{
    std::string wire;
    std::shared_ptr<protobuf::RemoteRequestResult> pbo(new protobuf::RemoteRequestResult());
    pbo->set_request_uid(request_uid);
    pbo->set_result(result);
    wire = ipaaca::converters::internal_serialize(pbo);
    IPAACA_DEBUG("Trying to send result to RemoteServer " << request_endpoint)
    int send_res = mosquittopp::publish(NULL, request_endpoint.c_str(), wire.size(), wire.c_str(), 2, false);
}

IPAACA_EXPORT void MQTTLocalServer::on_message(const struct mosquitto_message * message)
{
    auto event = ipaaca::converters::internal_deserialize(std::string((const char*) message->payload, message->payloadlen));
    auto type = event->getType();
    IPAACA_DEBUG("LocalServer " << _scope << " got an object of type " << type)
    int64_t result = 0;
    std::string request_endpoint("");
    std::string request_uid("");
    if (type == "ipaaca::IUPayloadUpdate") {
        auto obj = std::static_pointer_cast<ipaaca::IUPayloadUpdate>(event->getData());
        request_uid = obj->request_uid;
        request_endpoint = obj->request_endpoint;
        result = LocalServer::attempt_to_apply_remote_payload_update(obj);
    } else if (type == "ipaaca::IULinkUpdate") {
        auto obj = std::static_pointer_cast<ipaaca::IULinkUpdate>(event->getData());
        request_uid = obj->request_uid;
        result = LocalServer::attempt_to_apply_remote_link_update(obj);
    } else if (type == "ipaaca::protobuf::IUCommission") {
        auto obj = std::static_pointer_cast<ipaaca::protobuf::IUCommission>(event->getData());
        request_uid = obj->request_uid();
        result = LocalServer::attempt_to_apply_remote_commission(obj);
    } else if (type == "ipaaca::protobuf::IUResendRequest") {
        auto obj = std::static_pointer_cast<ipaaca::protobuf::IUResendRequest>(event->getData());
        request_uid = obj->request_uid();
        result = LocalServer::attempt_to_apply_remote_resend_request(obj);
    } else {
        IPAACA_ERROR("MQTTLocalServer: unhandled request wire type " << type)
    }
    if (request_uid.length()) {
        send_result_for_request(request_endpoint, request_uid, result);
    } else {
        IPAACA_ERROR("MQTTLocalServer: cannot reply since request_uid is unknown")
    }
}
//}}}

// MQTTRemoteServer{{{
// RemoteServer (= the side that sends requests to the owner of a remote IU)
IPAACA_EXPORT MQTTRemoteServer::MQTTRemoteServer(const std::string& client_id, const std::string& scope, Config::ptr config)
: MQTTParticipant(client_id, scope, config) {
    _remote_end_scope = _scope;
    auto uuid = ipaaca::generate_uuid_string().substr(0,8);
    _name = "LocalServer_" +  uuid; // TODO add e.g. pid as in Python
    _scope = "/ipaaca/remotes/" + _name; // overwrites constructed MQTTParticipant::_scope here (!)
    IPAACA_DEBUG("Create MQTTRemoteServer for remote scope " << ((std::string) _remote_end_scope) << " and reply listener on " << _scope)
    connect_and_background();
}
IPAACA_EXPORT int64_t MQTTRemoteServer::request_remote_payload_update(std::shared_ptr<IUPayloadUpdate> update)
{
    return blocking_call(std::make_shared<Event>("ipaaca::IUPayloadUpdate", update));
}
IPAACA_EXPORT int64_t MQTTRemoteServer::request_remote_link_update(std::shared_ptr<IULinkUpdate> update)
{
    return blocking_call(std::make_shared<Event>("ipaaca::IULinkUpdate", update));
}
IPAACA_EXPORT int64_t MQTTRemoteServer::request_remote_commission(std::shared_ptr<protobuf::IUCommission> update)
{
    return blocking_call(std::make_shared<Event>("ipaaca::protobuf::IUCommission", update));
}
IPAACA_EXPORT int64_t MQTTRemoteServer::request_remote_resend_request(std::shared_ptr<protobuf::IUResendRequest> update)
{
    return blocking_call(std::make_shared<Event>("ipaaca::protobuf::IUResendRequest", update));
}
IPAACA_EXPORT void MQTTRemoteServer::on_connect(int rc)
{
    int res = subscribe(NULL, _scope.c_str(), 2);
    if (res!=0) {
        IPAACA_ERROR("subscribe (on topic " << _scope << ") returned an error " << res)
    } else {
        IPAACA_DEBUG("subscribe returned OK for topic " << _scope)
    }
}

IPAACA_EXPORT void MQTTRemoteServer::on_subscribe(int mid, int qos_count, const int * granted_qos)
{
    if (qos_count < 1) {
        IPAACA_WARNING("No QoS grants reported")
    } else if (qos_count > 1) {
        IPAACA_WARNING("More than one QoS grant reported for Listener, should not happen")
    } else {
        int qos = granted_qos[0];
        if (qos!=2) {
            IPAACA_WARNING("MQTT QoS level 2 (guaranteed delivery) has NOT been granted on " << _scope << " (we got level " << qos << ")")
        }
    }
    signal_live();
}

IPAACA_EXPORT void MQTTRemoteServer::on_message(const struct mosquitto_message * message)
{
    auto event = ipaaca::converters::internal_deserialize(std::string((const char*) message->payload, message->payloadlen));
    auto type = event->getType();
    IPAACA_DEBUG("RemoteServer " << _scope << " for remote " << _remote_end_scope << " got an object of type " << type)
    if (type == "ipaaca::protobuf::RemoteRequestResult") {
        auto reply = std::static_pointer_cast<ipaaca::protobuf::RemoteRequestResult>(event->getData());
        auto uid = reply->request_uid();
        auto result = reply->result();
        PendingRequest::ptr pending_request;
        {
            ipaaca::Locker locker(_pending_requests_lock);
            auto it = _pending_requests.find(uid);
            if (it != _pending_requests.end()) {
                pending_request = it->second;
                _pending_requests.erase(it);
            }
        }
        if (pending_request) {
            pending_request->reply_with_result(result);
        } else {
            IPAACA_ERROR("MQTTRemoteServer: got a reply for a request that is not pending: " << uid)
        }
    } else {
        IPAACA_ERROR("MQTTRemoteServer: unhandled request wire type " << type)
    }
}

IPAACA_EXPORT PendingRequest::ptr MQTTRemoteServer::queue_pending_request(Event::ptr request)
{
    PendingRequest::ptr pending_request = std::make_shared<PendingRequest>(request);
    {
        ipaaca::Locker locker(_pending_requests_lock);
        if ((_MQTT_REMOTE_SERVER_MAX_QUEUED_REQUESTS > 0) && (_pending_requests.size() >= _MQTT_REMOTE_SERVER_MAX_QUEUED_REQUESTS)) {
            IPAACA_ERROR("MQTTRemoteServer: maximum number of pending requests exceeded")
            throw BackEndBadConditionError();
        } else {
            _pending_requests[pending_request->_request_uid] = pending_request;
            return pending_request;
        }
    }
}
IPAACA_EXPORT int64_t MQTTRemoteServer::blocking_call(Event::ptr request)
{
    std::string wire;
    auto pending_request = queue_pending_request(request);
    if (request->getType() == "ipaaca::IUPayloadUpdate") {
        auto obj = std::static_pointer_cast<ipaaca::IUPayloadUpdate>(request->getData());
        obj->request_uid = pending_request->_request_uid;
        obj->request_endpoint = _scope;
        wire = ipaaca::converters::internal_serialize(obj);
    } else if (request->getType() == "ipaaca::IULinkUpdate") {
        auto obj = std::static_pointer_cast<ipaaca::IULinkUpdate>(request->getData());
        obj->request_uid = pending_request->_request_uid;
        obj->request_endpoint = _scope;
        wire = ipaaca::converters::internal_serialize(obj);
    } else if (request->getType() == "ipaaca::protobuf::IUCommission") {
        auto obj = std::static_pointer_cast<ipaaca::protobuf::IUCommission>(request->getData());
        obj->set_request_uid(pending_request->_request_uid);
        obj->set_request_endpoint(_scope);
        wire = ipaaca::converters::internal_serialize(obj);
    } else if (request->getType() == "ipaaca::protobuf::IUResendRequest") {
        auto obj = std::static_pointer_cast<ipaaca::protobuf::IUResendRequest>(request->getData());
        obj->set_request_uid(pending_request->_request_uid);
        obj->set_request_endpoint(_scope);
        wire = ipaaca::converters::internal_serialize(obj);
    } else {
        IPAACA_ERROR("Unhandled request type " << request->getType())
        throw BackEndBadConditionError();
    }
    IPAACA_DEBUG("Trying to send request to remote LocalServer at " << _remote_end_scope)
    int send_res = mosquittopp::publish(NULL, _remote_end_scope.c_str(), wire.size(), wire.c_str(), 2, false);
    IPAACA_DEBUG("Waiting for the remote server")
    auto result = pending_request->wait_for_reply();
    IPAACA_DEBUG("RPC wait ended.")
    if (result<0) {
        IPAACA_WARNING("A RemoteServer request timed out, remote end was " << _remote_end_scope)
        return 0;
    } else {
        return result;
    }
}

//}}}


} // of namespace mqtt
} // of namespace backend
} // of namespace ipaaca

