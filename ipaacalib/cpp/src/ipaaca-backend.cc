/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca-backend.cc
 *
 * \brief Source file for abstract backend participant implementation
 *   (used in the core library and as a base to derive specific backends).
 *
 * \author Ramin Yaghoubzadeh Torky (ryaghoubzadeh@uni-bielefeld.de)
 * \date   December, 2018
 */

#include <ipaaca/ipaaca.h>

namespace ipaaca {
namespace backend {

// LocalServer (= the side that owns the actual IUs and tries to honor remote requests){{{
IPAACA_EXPORT int64_t LocalServer::attempt_to_apply_remote_payload_update(std::shared_ptr<IUPayloadUpdate> update)
{
	IUInterface::ptr iui = _buffer->get(update->uid);
	if (! iui) {
		IPAACA_WARNING("Remote InBuffer tried to spuriously write non-existent IU " << update->uid)
		return 0;
	}
	IU::ptr iu = std::static_pointer_cast<IU>(iui);
	iu->_revision_lock.lock();
	if ((update->revision != 0) && (update->revision != iu->_revision)) {
		IPAACA_WARNING("Remote write operation failed because request was out of date; IU " << update->uid)
		IPAACA_WARNING(" Referred-to revision was " << update->revision << " while local one is " << iu->_revision)
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->committed()) {
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->retracted()) {
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->committed()) {
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->retracted()) {
		iu->_revision_lock.unlock();
		return 0;
	}
	if (update->is_delta) {
		// FIXME TODO this is an unsolved problem atm: deletions in a delta update are
		// sent individually. We should have something like _internal_merge_and_remove
		for (std::vector<std::string>::const_iterator it=update->keys_to_remove.begin(); it!=update->keys_to_remove.end(); ++it) {
			iu->payload()._internal_remove(*it, update->writer_name); //_buffer->unique_name());
		}
		// but it is solved for pure merges:
		iu->payload()._internal_merge(update->new_items, update->writer_name);
	} else {
		iu->payload()._internal_replace_all(update->new_items, update->writer_name); //_buffer->unique_name());
	}
	_buffer->call_iu_event_handlers(iu, true, IU_UPDATED, iu->category());
	revision_t revision = iu->revision();
	iu->_revision_lock.unlock();
	return revision;
}

IPAACA_EXPORT int64_t LocalServer::attempt_to_apply_remote_link_update(std::shared_ptr<IULinkUpdate> update)
{
	IUInterface::ptr iui = _buffer->get(update->uid);
	if (! iui) {
		IPAACA_WARNING("Remote InBuffer tried to spuriously write non-existent IU " << update->uid)
		return 0;
	}
	IU::ptr iu = std::static_pointer_cast<IU>(iui);
	iu->_revision_lock.lock();
	if ((update->revision != 0) && (update->revision != iu->_revision)) {
		IPAACA_WARNING("Remote write operation failed because request was out of date; IU " << update->uid)
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->committed()) {
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->retracted()) {
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->committed()) {
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->retracted()) {
		iu->_revision_lock.unlock();
		return 0;
	}
	if (update->is_delta) {
		iu->modify_links(update->new_links, update->links_to_remove, update->writer_name);
	} else {
		iu->set_links(update->new_links, update->writer_name);
	}
	_buffer->call_iu_event_handlers(iu, true, IU_LINKSUPDATED, iu->category());
	revision_t revision = iu->revision();
	iu->_revision_lock.unlock();
	return revision;
}
IPAACA_EXPORT int64_t LocalServer::attempt_to_apply_remote_commission(std::shared_ptr<protobuf::IUCommission> update)
{
	IUInterface::ptr iui = _buffer->get(update->uid());
	if (! iui) {
		IPAACA_WARNING("Remote InBuffer tried to spuriously write non-existent IU " << update->uid())
		return 0;
	}
	IU::ptr iu = std::static_pointer_cast<IU>(iui);
	iu->_revision_lock.lock();
	if ((update->revision() != 0) && (update->revision() != iu->_revision)) {
		IPAACA_WARNING("Remote write operation failed because request was out of date; IU " << update->uid())
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->committed()) {
		iu->_revision_lock.unlock();
		return 0;
	} else if (iu->retracted()) {
		iu->_revision_lock.unlock();
		return 0;
	} else {
	}
	iu->_internal_commit(update->writer_name());
	_buffer->call_iu_event_handlers(iu, true, IU_LINKSUPDATED, iu->category());
	revision_t revision = iu->revision();
	iu->_revision_lock.unlock();
	return revision;
}
IPAACA_EXPORT int64_t LocalServer::attempt_to_apply_remote_resend_request(std::shared_ptr<protobuf::IUResendRequest> update)
{
	IUInterface::ptr iui = _buffer->get(update->uid());
	if (! iui) {
		IPAACA_WARNING("Remote InBuffer tried to spuriously write non-existent IU " << update->uid())
		return 0;
	}
	IU::ptr iu = std::static_pointer_cast<IU>(iui);
	if ((update->has_hidden_scope_name() == true)&&(update->hidden_scope_name().compare("") != 0)){
		revision_t revision = iu->revision();
		_buffer->_publish_iu_resend(iu, update->hidden_scope_name());
		return revision;
	} else {
		revision_t revision = 0;
		return revision;
	}
}
//}}}


IPAACA_EXPORT void Listener::relay_received_event_to_buffer(Event::ptr event)
{
    //std::cout << "Will relay it" << std::endl;
    _buffer->_handle_iu_events(event);
}
IPAACA_EXPORT void Listener::relay_received_event_to_buffer_threaded(Event::ptr event)
{
    auto buffer = _buffer; // avoid a 'this' lambda capture
    std::thread dispatcher(
            [buffer,event] () {
                    buffer->_handle_iu_events(event);
            });
    dispatcher.detach();
}


} // of namespace backend
} // of namespace ipaaca

