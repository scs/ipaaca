/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

#include <ipaaca/ipaaca.h>

namespace ipaaca {
namespace converters {

// Wrap a serialized inner object and a wire type in a protobuf::TransportLevelWrapper
std::string cooked_message(const std::string& raw_message, ipaaca::protobuf::TransportMessageType msg_type)
{
    std::string cooked_msg;
    std::shared_ptr<protobuf::TransportLevelWrapper> pbo(new protobuf::TransportLevelWrapper());
    pbo->set_raw_message(raw_message);
    pbo->set_transport_message_type(msg_type);
    pbo->SerializeToString(&cooked_msg);
    return cooked_msg;
};

// protobuf serialization for all supported types (replaces converter repository)
std::string internal_serialize(ipaaca::IU::ptr iu) {
    std::string raw_message = IUConverter::serialize(iu);
    return cooked_message(raw_message,
            (iu->access_mode()==IU_ACCESS_MESSAGE) ? protobuf::TransportMessageType::WireTypeMessageIU
                                                   : protobuf::TransportMessageType::WireTypeIU);
};
/*
std::string internal_serialize(ipaaca::Message::ptr msg) {
    std::string raw_message = MessageConverter::serialize(msg);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeMessageIU);
};
*/
std::string internal_serialize(ipaaca::IUPayloadUpdate::ptr pup) {
    std::string raw_message = IUPayloadUpdateConverter::serialize(pup);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeIUPayloadUpdate);
};
std::string internal_serialize(ipaaca::IULinkUpdate::ptr lup) {
    std::string raw_message = IULinkUpdateConverter::serialize(lup);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeIULinkUpdate);
};
std::string internal_serialize(std::shared_ptr<protobuf::RemoteRequestResult> pb) {
    std::string raw_message;
    pb->SerializeToString(&raw_message);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeRemoteRequestResult);
};
std::string internal_serialize(std::shared_ptr<protobuf::IURetraction> pb) {
    std::string raw_message;
    pb->SerializeToString(&raw_message);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeIURetraction);
};
std::string internal_serialize(std::shared_ptr<protobuf::IUCommission> pb) {
    std::string raw_message;
    pb->SerializeToString(&raw_message);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeIUCommission);
};
std::string internal_serialize(std::shared_ptr<protobuf::IUResendRequest> pb) {
    std::string raw_message;
    pb->SerializeToString(&raw_message);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeIUResendRequest);
};
std::string internal_serialize(std::shared_ptr<protobuf::IUPayloadUpdateRequest> pb) {
    std::string raw_message;
    pb->SerializeToString(&raw_message);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeIUPayloadUpdateRequest);
};
std::string internal_serialize(std::shared_ptr<protobuf::IULinkUpdateRequest> pb) {
    std::string raw_message;
    pb->SerializeToString(&raw_message);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeIULinkUpdateRequest);
};
std::string internal_serialize(std::shared_ptr<protobuf::IUCommissionRequest> pb) {
    std::string raw_message;
    pb->SerializeToString(&raw_message);
    return cooked_message(raw_message, protobuf::TransportMessageType::WireTypeIUCommissionRequest);
};

// deserialization (just switching here instead of the converter registry business)
ipaaca::backend::Event::ptr internal_deserialize(const std::string& wire)
{
    std::shared_ptr<protobuf::TransportLevelWrapper> pbo(new protobuf::TransportLevelWrapper());
    pbo->ParseFromString(wire);
    std::shared_ptr<ipaaca::backend::Event> event;
    //std::cout << "internal_deserialize of TransportMessageType " << pbo->transport_message_type() << std::endl;
    switch (pbo->transport_message_type()) {
        case protobuf::TransportMessageType::WireTypeIU:
            { event = std::make_shared<ipaaca::backend::Event>("ipaaca::RemotePushIU", std::static_pointer_cast<RemotePushIU>(IUConverter::deserialize(pbo->raw_message()))); }
            break;
        case protobuf::TransportMessageType::WireTypeMessageIU:
            { event = std::make_shared<ipaaca::backend::Event>("ipaaca::RemoteMessage", std::static_pointer_cast<RemoteMessage>(IUConverter::deserialize(pbo->raw_message()))); }
            break;
        case protobuf::TransportMessageType::WireTypeIUPayloadUpdate:
            { event = std::make_shared<ipaaca::backend::Event>("ipaaca::IUPayloadUpdate", IUPayloadUpdateConverter::deserialize(pbo->raw_message())); }
            break;
        case protobuf::TransportMessageType::WireTypeIULinkUpdate:
            { event = std::make_shared<ipaaca::backend::Event>("ipaaca::IULinkUpdate", IULinkUpdateConverter::deserialize(pbo->raw_message())); }
            break;
        case protobuf::TransportMessageType::WireTypeIURetraction:
            { 
                std::shared_ptr<protobuf::IURetraction> inner(new protobuf::IURetraction());
                inner->ParseFromString(pbo->raw_message());
                event = std::make_shared<ipaaca::backend::Event>("ipaaca::protobuf::IURetraction", inner);
            }
            break;
        case protobuf::TransportMessageType::WireTypeIUCommission:
            {
                std::shared_ptr<protobuf::IUCommission> inner(new protobuf::IUCommission());
                inner->ParseFromString(pbo->raw_message());
                event = std::make_shared<ipaaca::backend::Event>("ipaaca::protobuf::IUCommission", inner);
            }
            break;
        case protobuf::TransportMessageType::WireTypeRemoteRequestResult:
            {
                std::shared_ptr<protobuf::RemoteRequestResult> inner(new protobuf::RemoteRequestResult());
                inner->ParseFromString(pbo->raw_message());
                event = std::make_shared<ipaaca::backend::Event>("ipaaca::protobuf::RemoteRequestResult", inner);
            }
            break;
        case protobuf::TransportMessageType::WireTypeIUResendRequest:
            {
                std::shared_ptr<protobuf::IUResendRequest> inner(new protobuf::IUResendRequest());
                inner->ParseFromString(pbo->raw_message());
                event = std::make_shared<ipaaca::backend::Event>("ipaaca::protobuf::IUResendRequest", inner);
            }
            break;
        case protobuf::TransportMessageType::WireTypeIUPayloadUpdateRequest:
            {
                std::shared_ptr<protobuf::IUPayloadUpdateRequest> inner(new protobuf::IUPayloadUpdateRequest());
                inner->ParseFromString(pbo->raw_message());
                event = std::make_shared<ipaaca::backend::Event>("ipaaca::protobuf::IUPayloadUpdateRequest", inner);
            }
            break;
        case protobuf::TransportMessageType::WireTypeIULinkUpdateRequest:
            {
                std::shared_ptr<protobuf::IULinkUpdateRequest> inner(new protobuf::IULinkUpdateRequest());
                inner->ParseFromString(pbo->raw_message());
                event = std::make_shared<ipaaca::backend::Event>("ipaaca::protobuf::IULinkUpdateRequest", inner);
            }
            break;
        case protobuf::TransportMessageType::WireTypeIUCommissionRequest:
            {
                std::shared_ptr<protobuf::IUCommissionRequest> inner(new protobuf::IUCommissionRequest());
                inner->ParseFromString(pbo->raw_message());
                event = std::make_shared<ipaaca::backend::Event>("ipaaca::protobuf::IUCommissionRequest", inner);
            }
            break;
        default:
            throw ipaaca::UnhandledWireTypeError(pbo->transport_message_type());
    };
    return event;
}

// RSB backend Converters
// IUConverter//{{{

IPAACA_EXPORT std::string IUConverter::serialize(ipaaca::IU::ptr obj)
{
        std::string wire;
	std::shared_ptr<protobuf::IU> pbo(new protobuf::IU());
	// transfer obj data to pbo
	pbo->set_uid(obj->uid());
	pbo->set_revision(obj->revision());
	pbo->set_category(obj->category());
	pbo->set_payload_type(obj->payload_type());
	pbo->set_owner_name(obj->owner_name());
	pbo->set_committed(obj->committed());
	ipaaca::protobuf::IU_AccessMode a_m;
	switch(obj->access_mode()) {
		case IU_ACCESS_PUSH:
			a_m = ipaaca::protobuf::IU_AccessMode_PUSH;
			break;
		case IU_ACCESS_REMOTE:
			a_m = ipaaca::protobuf::IU_AccessMode_REMOTE;
			break;
		case IU_ACCESS_MESSAGE:
			a_m = ipaaca::protobuf::IU_AccessMode_MESSAGE;
			break;
	}
	pbo->set_access_mode(a_m);
	pbo->set_read_only(obj->read_only());
	for (auto& kv: obj->_payload._document_store) {
		protobuf::PayloadItem* item = pbo->add_payload();
		item->set_key(kv.first);
		IPAACA_DEBUG("Payload type: " << obj->_payload_type)
		if (obj->_payload_type=="JSON") {
			item->set_value( kv.second->to_json_string_representation() );
			item->set_type("JSON");
		} else if ((obj->_payload_type=="MAP") || (obj->_payload_type=="STR")) {
			// legacy mode
			item->set_value( json_value_cast<std::string>(kv.second->document));
			item->set_type("STR");
		}
	}
	for (LinkMap::const_iterator it=obj->_links._links.begin(); it!=obj->_links._links.end(); ++it) {
		protobuf::LinkSet* links = pbo->add_links();
		links->set_type(it->first);
		for (std::set<std::string>::const_iterator it2=it->second.begin(); it2!=it->second.end(); ++it2) {
			links->add_targets(*it2);
		}
	}
	pbo->SerializeToString(&wire);
        return wire;
}

IPAACA_EXPORT ipaaca::IUInterface::ptr IUConverter::deserialize(const std::string& wire) {
	//assert(wireSchema == getWireSchema()); // "ipaaca-iu"
	std::shared_ptr<protobuf::IU> pbo(new protobuf::IU());
	pbo->ParseFromString(wire);
	IUAccessMode mode = static_cast<IUAccessMode>(pbo->access_mode());
        ipaaca::IUInterface::ptr obj;
        switch(mode) {
		case IU_ACCESS_PUSH:
			{
                            // Create a "remote push IU"
                            auto inst = RemotePushIU::create();
                            inst->_access_mode = IU_ACCESS_PUSH;
                            obj = inst;
                            for (int i=0; i<pbo->payload_size(); i++) {
                                    const protobuf::PayloadItem& it = pbo->payload(i);
                                    PayloadDocumentEntry::ptr entry;
                                    if (it.type() == "JSON") {
                                            // fully parse json text
                                            entry = PayloadDocumentEntry::from_json_string_representation( it.value() );
                                    } else {
                                            // assuming legacy "str" -> just copy value to raw string in document
                                            entry = std::make_shared<PayloadDocumentEntry>();
                                            entry->document.SetString(it.value(), entry->document.GetAllocator());
                                    }
                                    inst->_payload._document_store[it.key()] = entry;
                            }
                        }
                        break;
		case IU_ACCESS_MESSAGE:
                        {
                            auto inst = RemoteMessage::create();
                            inst->_access_mode = IU_ACCESS_MESSAGE;
                            obj = inst;
                            for (int i=0; i<pbo->payload_size(); i++) {
                                    const protobuf::PayloadItem& it = pbo->payload(i);
                                    PayloadDocumentEntry::ptr entry;
                                    if (it.type() == "JSON") {
                                            // fully parse json text
                                            entry = PayloadDocumentEntry::from_json_string_representation( it.value() );
                                    } else {
                                            // assuming legacy "str" -> just copy value to raw string in document
                                            entry = std::make_shared<PayloadDocumentEntry>();
                                            entry->document.SetString(it.value(), entry->document.GetAllocator());
                                    }
                                    inst->_payload._document_store[it.key()] = entry;
                            }
                        }
			break;
                default:
                        throw NotImplementedError();
        }
        // transfer pbo data to obj
        obj->_uid = pbo->uid();
        obj->_revision = pbo->revision();
        obj->_category = pbo->category();
        obj->_payload_type = pbo->payload_type();
        obj->_owner_name = pbo->owner_name();
        obj->_committed = pbo->committed();
        obj->_read_only = pbo->read_only();
        for (int i=0; i<pbo->links_size(); i++) {
                const protobuf::LinkSet& pls = pbo->links(i);
                LinkSet& ls = obj->_links._links[pls.type()];
                for (int j=0; j<pls.targets_size(); j++) {
                        ls.insert(pls.targets(j));
                }
        }
        return obj;
}

//}}}

// IUPayloadUpdateConverter//{{{

IPAACA_EXPORT std::string IUPayloadUpdateConverter::serialize(ipaaca::IUPayloadUpdate::ptr obj)
{
        std::string wire;
	//assert(data.first == getDataType()); // "ipaaca::IUPayloadUpdate"
	std::shared_ptr<protobuf::IUPayloadUpdate> pbo(new protobuf::IUPayloadUpdate());
	// transfer obj data to pbo
	pbo->set_uid(obj->uid);
	pbo->set_revision(obj->revision);
	pbo->set_writer_name(obj->writer_name);
	pbo->set_is_delta(obj->is_delta);
        pbo->set_request_uid(obj->request_uid);
        pbo->set_request_endpoint(obj->request_endpoint);
	for (auto& kv: obj->new_items) {
		protobuf::PayloadItem* item = pbo->add_new_items();
		item->set_key(kv.first);
		if (obj->payload_type=="JSON") {
			item->set_value( kv.second->to_json_string_representation() );
			item->set_type("JSON");
		} else if ((obj->payload_type=="MAP") || (obj->payload_type=="STR")) {
			// legacy mode
			item->set_value( json_value_cast<std::string>(kv.second->document));
			item->set_type("STR");
		} else {
			IPAACA_ERROR("Uninitialized payload update type!")
			throw NotImplementedError();
		}
		IPAACA_DEBUG("Adding updated item (type " << item->type() << "): " << item->key() << " -> " << item->value() )
	}
	for (auto& key: obj->keys_to_remove) {
		pbo->add_keys_to_remove(key);
		IPAACA_DEBUG("Adding removed key: " << key)
	}
	pbo->SerializeToString(&wire);
        return wire;
}

ipaaca::IUPayloadUpdate::ptr IUPayloadUpdateConverter::deserialize(const std::string& wire) {
	//assert(wireSchema == getWireSchema()); // "ipaaca-iu-payload-update"
	std::shared_ptr<protobuf::IUPayloadUpdate> pbo(new protobuf::IUPayloadUpdate());
	pbo->ParseFromString(wire);
	std::shared_ptr<IUPayloadUpdate> obj(new IUPayloadUpdate());
	// transfer pbo data to obj
	obj->uid = pbo->uid();
	obj->revision = pbo->revision();
	obj->writer_name = pbo->writer_name();
	obj->is_delta = pbo->is_delta();
        obj->request_uid = pbo->request_uid();
        obj->request_endpoint = pbo->request_endpoint();
	for (int i=0; i<pbo->new_items_size(); i++) {
		const protobuf::PayloadItem& it = pbo->new_items(i);
		PayloadDocumentEntry::ptr entry;
		if (it.type() == "JSON") {
			// fully parse json text
			entry = PayloadDocumentEntry::from_json_string_representation( it.value() );
			IPAACA_DEBUG("New/updated payload entry: " << it.key() << " -> " << it.value() )
		} else {
			// assuming legacy "str" -> just copy value to raw string in document
			entry = std::make_shared<PayloadDocumentEntry>();
			entry->document.SetString(it.value(), entry->document.GetAllocator());
		}
		obj->new_items[it.key()] = entry;
	}
	for (int i=0; i<pbo->keys_to_remove_size(); i++) {
		obj->keys_to_remove.push_back(pbo->keys_to_remove(i));
	}
	return obj;
}

//}}}
// IULinkUpdateConverter//{{{

IPAACA_EXPORT std::string IULinkUpdateConverter::serialize(ipaaca::IULinkUpdate::ptr obj)
{
        std::string wire;
	//assert(data.first == getDataType());
	std::shared_ptr<protobuf::IULinkUpdate> pbo(new protobuf::IULinkUpdate());
	// transfer obj data to pbo
	pbo->set_uid(obj->uid);
	pbo->set_revision(obj->revision);
	pbo->set_writer_name(obj->writer_name);
	pbo->set_is_delta(obj->is_delta);
        pbo->set_request_uid(obj->request_uid);
        pbo->set_request_endpoint(obj->request_endpoint);
	for (std::map<std::string, std::set<std::string> >::const_iterator it=obj->new_links.begin(); it!=obj->new_links.end(); ++it) {
		protobuf::LinkSet* links = pbo->add_new_links();
		links->set_type(it->first);
		for (std::set<std::string>::const_iterator it2=it->second.begin(); it2!=it->second.end(); ++it2) {
			links->add_targets(*it2);
		}
	}
	for (std::map<std::string, std::set<std::string> >::const_iterator it=obj->links_to_remove.begin(); it!=obj->links_to_remove.end(); ++it) {
		protobuf::LinkSet* links = pbo->add_links_to_remove();
		links->set_type(it->first);
		for (std::set<std::string>::const_iterator it2=it->second.begin(); it2!=it->second.end(); ++it2) {
			links->add_targets(*it2);
		}
	}
	pbo->SerializeToString(&wire);
        return wire;
}

ipaaca::IULinkUpdate::ptr IULinkUpdateConverter::deserialize(const std::string& wire) {
	//assert(wireSchema == getWireSchema()); // "ipaaca-iu-link-update"
	std::shared_ptr<protobuf::IULinkUpdate> pbo(new protobuf::IULinkUpdate());
	pbo->ParseFromString(wire);
	std::shared_ptr<IULinkUpdate> obj(new IULinkUpdate());
	// transfer pbo data to obj
	obj->uid = pbo->uid();
	obj->revision = pbo->revision();
	obj->writer_name = pbo->writer_name();
	obj->is_delta = pbo->is_delta();
        obj->request_uid = pbo->request_uid();
        obj->request_endpoint = pbo->request_endpoint();
	for (int i=0; i<pbo->new_links_size(); ++i) {
		const protobuf::LinkSet& it = pbo->new_links(i);
		for (int j=0; j<it.targets_size(); ++j) {
			obj->new_links[it.type()].insert(it.targets(j)); // = vec;
		}
	}
	for (int i=0; i<pbo->links_to_remove_size(); ++i) {
		const protobuf::LinkSet& it = pbo->links_to_remove(i);
		for (int j=0; j<it.targets_size(); ++j) {
			obj->links_to_remove[it.type()].insert(it.targets(j));
		}
	}
	return obj;
}

//}}}


} // namespace converters
} // namespace ipaaca

