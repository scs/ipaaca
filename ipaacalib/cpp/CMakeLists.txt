cmake_minimum_required (VERSION 2.6)

# project name
project (ipaaca_cpp)

#set (CMAKE_CXX_STANDARD 11)

## use C++11 (starting with proto v2 / ipaaca-c++ release 12)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
## use the following line to enable console debug messages in ipaaca
##  (this entails a slight slowdown since dynamic log level checks made at the very least)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DIPAACA_DEBUG_MESSAGES")
# expose the full RSB api in the headers (set only in ipaaca itself)
#  !! NOTE: at the moment required in any ipaaca cpp project in Windows !!
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DIPAACA_EXPOSE_FULL_RSB_API")
## use the following line to enable building mock IUs (FakeIU)
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DIPAACA_BUILD_MOCK_OBJECTS")
# find cmake modules locally too
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/CMakeModules )

if(WIN32) # Check if we are on Windows
	if(MSVC) # Check if we are using the Visual Studio compiler
		#set_target_properties(TestProject PROPERTIES LINK_FLAGS_RELEASE "/SUBSYSTEM:WINDOWS")
		#
		#   Setup section for Windows build (using precompiled rsb + deps)
		#
		#   You need the rsx precompiled archive, even if you build rsb yourself,
		#   for the dependencies. Make sure to grab the right version (bitness
		#   and Visual Studio version). Tested with the rsx-0.10 branch.
		#   Please unpack the rsx archive into the repo dir (where ipaaca also is).
		#   Then set these environment variables before building rsb or ipaaca:
		#
		#   set BOOST_ROOT=%SOA_REPO_DIR%\rsx\boost
		#   set PROTOBUF_ROOT=%SOA_REPO_DIR%\rsx\protobuf
		#   set SPREAD_ROOT=%SOA_REPO_DIR%\rsx\spread
		#
		#
		
		set(LIBS ${LIBS} rpcrt4)

		#set(Boost_USE_STATIC_LIBS ON)
		#set(Boost_USE_MULTITHREADED ON)
	
		#find_package(Boost COMPONENTS date_time program_options system filesystem thread signals regex REQUIRED)
		#link_directories(${Boost_LIBRARY_DIRS})
		#include_directories(${Boost_INCLUDE_DIRS})
		
		## Windows linkage hack: overriding the determined libs to remove boost_thread (causes multiple-definition issues)
		#set(CORRECT_BOOST_LIBS "")
		#foreach(BLIB ${Boost_LIBRARIES})
		#			list(APPEND CORRECT_BOOST_LIBS ${BLIB})
		#endforeach(BLIB ${Boost_LIBRARIES})
		#set(Boost_LIBRARIES ${CORRECT_BOOST_LIBS})
		
		# Using custom Protobuf script (from rsc) because it honors PROTOBUF_ROOT
		#find_package(ProtocolBuffers REQUIRED PATHS "\\Libs\\protobuf")
		#link_directories(${PROTOBUF_LIBRARY_DIRS})
		#include_directories(${PROTOBUF_INCLUDE_DIRS})
		#message(STATUS "############################")
		#message(STATUS ${PROTOBUF_INCLUDE_DIRS})
		set(PROTOBUF_LIBRARY optimized "C:\\libs\\protobuf\\x64\\lib\\libprotobuf.lib" debug "C:\\libs\\protobuf\\x64\\lib\\libprotobufd.lib")
		link_directories( "C:\\libs\\protobuf\\x64\\lib" )
		include_directories( "C:\\libs\\protobuf\\x64\\include" )
		
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MT")
		set(CompilerFlags
			CMAKE_CXX_FLAGS
			CMAKE_CXX_FLAGS_DEBUG
			CMAKE_CXX_FLAGS_RELEASE
			CMAKE_C_FLAGS
			CMAKE_C_FLAGS_DEBUG
			CMAKE_C_FLAGS_RELEASE
			)
		foreach(CompilerFlag ${CompilerFlags})
		  string(REPLACE "/MD" "/MT" ${CompilerFlag} "${${CompilerFlag}}")
		endforeach()
			
	else()
		message(SEND_ERROR "Unsupported compiler! Please build with MSVC++ 19.x (Visual Studio 2017).")
	endif()
else()
	#
	#
	# Setup section for Linux or OS X (using 'rsb' soa project)
	#
	#
	find_package(Boost COMPONENTS system filesystem thread regex REQUIRED)
	link_directories(${Boost_LIBRARY_DIRS})
	include_directories(${Boost_INCLUDE_DIRS})
	#set(BOOSTLIBS boost_regex-mt boost_date_time-mt boost_program_options-mt boost_thread-mt boost_filesystem-mt boost_signals-mt boost_system-mt)

	find_package(ProtocolBuffers REQUIRED)
	link_directories(${PROTOBUF_LIBRARY_DIRS})
	include_directories(${PROTOBUF_INCLUDE_DIRS})

    set(PROTOBUF_INPUT_DIRECTORY "${PROJECT_SOURCE_DIR}")
    set(PROTOBUF_OUTPUT_DIRECTORY "${PROJECT_SOURCE_DIR}/build/ipaaca")
    file(MAKE_DIRECTORY ${PROTOBUF_OUTPUT_DIRECTORY})
    set(PROTOBUF_ARGUMENTS "--cpp_out=${PROTOBUF_OUTPUT_DIRECTORY}")
    execute_process(COMMAND protoc ${PROTOBUF_ARGUMENTS} ipaaca.proto
            WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}/../proto/
            RESULT_VARIABLE PROTOBUF_RESULT
            OUTPUT_VARIABLE PROTOBUF_OUTPUT_VARIABLE)

	# change for each new rsb version
        if (DEFINED APPLE)
        	#set(RSBLIBS rsc0.14 rsb0.14)
        	#set(RSBLIBS rsc0.10 rsb.0.10)
        else(DEFINED APPLE)
		#set(RSBLIBS ${PROJECT_SOURCE_DIR}/../../deps/lib/librsc0.14.so ${PROJECT_SOURCE_DIR}/../../deps/lib/librsb0.14.so )
		set(LIBS ${LIBS} uuid)
	endif(DEFINED APPLE)
	# enhance the default search paths (headers, libs ...)
	set(CMAKE_PREFIX_PATH ${PROJECT_SOURCE_DIR}:/opt/local:${CMAKE_PREFIX_PATH})
	# MacPorts compatibility
	if (DEFINED APPLE)
		message(STATUS "Adding extra options for building on Mac OS X")
		set(CXX_DEFINES "${CXX_DEFINES} -D__MACOSX__")
		link_directories( /opt/local/lib )
		include_directories( /opt/local/include )
	endif(DEFINED APPLE)
endif(WIN32)

set(LIBS ${LIBS} ${PROTOBUF_LIBRARY} ${Boost_LIBRARIES})


if(NOT DEFINED WIN32)
	if (DEFINED APPLE)
		message(STATUS "No extra pthread flags needed on Mac")
	else()
		message(STATUS "Adding extra pthread flags for Linux")
		# needs -lpthread AND -pthread
		set(LIBS ${LIBS} pthread)
		set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")
	endif()
endif(NOT DEFINED WIN32)

# Hide the rsb-induced boost-signals warning (FOR NOW)
#set(IPAACA_CXX_DEFINES "${IPAACA_CXX_DEFINES} -DBOOST_SIGNALS_NO_DEPRECATION_WARNING")

## Compiler defines copied from the old build system
#set(IPAACA_CXX_DEFINES "${IPAACA_CXX_DEFINES} -D_DEFAULT_SOURCE -DUSE_AV -DMGC_USE_DOUBLE -DLEDA_PREFIX -D__NO_CAST_TO_LOCAL_TYPE__ -DDBGLVL=0")

# Combine the extra compiler flags
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${CXX_OLD_CODE_CONVENIENCE_FLAGS} ${IPAACA_CXX_DEFINES}")

# add include dir for auto-generated headers placed in build/
include_directories( ${PROJECT_SOURCE_DIR}/build )

# add local include directory
include_directories( ${PROJECT_SOURCE_DIR}/include )
# add lib and include directory from pulled dependencies
include_directories( ${PROJECT_SOURCE_DIR}/../../deps/include )
link_directories( ${PROJECT_SOURCE_DIR}/../../deps/lib )

###
### Back-ends  (will include any and all that are compileable)
###
#
if(WIN32)
if(MSVC)
# fixed configuration for Windows
set(LIBS ${LIBS} "C:\\libs\\mosquitto\\x64\\mosquitto.lib" "C:\\libs\\mosquitto\\x64\\mosquittopp.lib")
link_directories( "C:\\libs\\mosquitto\\x64" )
include_directories( "C:\\libs\\mosquitto\\x64\\include" )
set(BACKEND_SOURCES ${BACKEND_SOURCES} src/ipaaca-backend-mqtt.cc)
endif(MSVC)
else()
# dynamic checks under Unix

# 1: MQTT
find_library(MOSQUITTO_LIB mosquittopp)
if(MOSQUITTO_LIB)
    message(STATUS "=== MQTT backend ===    enabled")
    set(LIBS ${LIBS} mosquittopp)
    set(BACKEND_SOURCES ${BACKEND_SOURCES} src/ipaaca-backend-mqtt.cc)
else()
    message(STATUS "=== MQTT backend ===    DISABLED (mosquittopp not found)")
endif()

# 2: ROS
set (ROSBASE $ENV{ROS_ROOT}/../..)
find_library(ROSCPP_LIB roscpp PATHS ${ROSBASE})
if(ROSCPP_LIB)
    message(STATUS "=== ROS backend ===    enabled")
    include_directories($ENV{ROS_ROOT}/../../include)
    link_directories($ENV{ROS_ROOT}/../../lib)
    set(LIBS ${LIBS} roscpp roscpp_serialization rosconsole)
    set(BACKEND_SOURCES ${BACKEND_SOURCES} src/ipaaca-backend-ros.cc)
else()
    message(STATUS "=== ROS backend ===    DISABLED (roscpp not found)")
endif()

#
# Now make sure that at least one of those backends is available
#
if(NOT BACKEND_SOURCES)
    message(SEND_ERROR "\nWill not build IPAACA without any backends!\n")
endif()

endif() # of Unix block

# specify source files for ipaaca (auto-generated ones are in build/ )
set (SOURCE
	src/ipaaca.cc
        src/ipaaca-config.cc
        src/ipaaca-converters.cc
        src/ipaaca-backend.cc
	src/ipaaca-buffers.cc
	src/ipaaca-initializer.cc
        src/ipaaca-iuinterface.cc
	src/ipaaca-ius.cc
	src/ipaaca-links.cc
	src/ipaaca-locking.cc
	src/ipaaca-payload.cc
	src/ipaaca-cmdline-parser.cc
	src/ipaaca-string-utils.cc
	src/util/notifier.cc
        src/b64/b64.cc
	build/ipaaca/ipaaca.pb.cc
	)
set (SOURCE ${SOURCE} ${BACKEND_SOURCES})

#set (JSON_TEST_SOURCE
#	src/ipaaca.cc
#	src/ipaaca-buffers.cc
#	src/ipaaca-fake.cc
#	src/ipaaca-internal.cc
#	src/ipaaca-iuinterface.cc
#	src/ipaaca-json.cc    # main
#	src/ipaaca-locking.cc
#	src/ipaaca-links.cc
#	src/ipaaca-payload.cc
#	src/ipaaca-cmdline-parser.cc
#	src/ipaaca-string-utils.cc
#	# more stuff going beyond the fake test case
#	src/ipaaca-ius.cc
#	build/ipaaca/ipaaca.pb.cc
#	)
#
set (TESTER_SOURCE
	src/ipaaca-tester.cc    # main
	src/ipaaca.cc
        src/ipaaca-config.cc
        src/ipaaca-converters.cc
        src/ipaaca-backend.cc
	src/ipaaca-buffers.cc
	src/ipaaca-initializer.cc
	src/ipaaca-iuinterface.cc
	src/ipaaca-ius.cc
	src/ipaaca-links.cc
	src/ipaaca-locking.cc
	src/ipaaca-payload.cc
	src/ipaaca-cmdline-parser.cc
	src/ipaaca-string-utils.cc
	src/util/notifier.cc
        src/b64/b64.cc
	build/ipaaca/ipaaca.pb.cc
	)
set (TESTER_SOURCE ${TESTER_SOURCE} ${BACKEND_SOURCES})

if(WIN32)
if(MSVC)
  if(NOT DEFINED CMAKE_INSTALL_PREFIX)
    set(CMAKE_INSTALL_PREFIX "C:/Libs/ipaaca/x64")
  endif()
endif()
else()
  # this is for building with cmake/soa
  set(CMAKE_INSTALL_PREFIX "")
endif()

# compile all files to "ipaaca" shared library
add_library(ipaaca SHARED ${SOURCE})
# and link all the required external libs (found above using find_package etc.)
target_link_libraries(ipaaca ${LIBS})

#add_executable (ipaaca-test-json ${JSON_TEST_SOURCE})
#target_link_libraries (ipaaca-test-json ${LIBS})

add_executable (ipaaca-tester-cpp ${TESTER_SOURCE})
target_compile_options (ipaaca-tester-cpp PRIVATE "-DIPAACA_STATIC_BINARY")
target_link_libraries (ipaaca-tester-cpp ${LIBS})

set(DEFAULT_BIN_SUBDIR bin)
set(DEFAULT_LIB_SUBDIR lib)
set(DEFAULT_DATA_SUBDIR share/data)
set(DEFAULT_INCLUDE_SUBDIR include)
install (
	TARGETS ipaaca ipaaca-tester-cpp
	RUNTIME DESTINATION bin
	LIBRARY DESTINATION lib
	ARCHIVE DESTINATION lib
	)
install(
	DIRECTORY include
	DESTINATION .
	FILES_MATCHING PATTERN "*.h" PATTERN "*.hh" PATTERN "*.hpp" PATTERN "*.inl"
	)
install(
	FILES build/ipaaca/ipaaca.pb.h
	DESTINATION include/ipaaca/
	)


