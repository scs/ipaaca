/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca-backend.h
 *
 * \brief Header file for abstract backend participant implementation
 *   (used in the core library and as a base to derive specific backends).
 *
 * Users should not include this file directly, but use ipaaca.h
 *
 * \b Note: This file is only included during compilation of ipaaca,
 * for regular use, the full internal API is not exposed.
 * Users generally need never touch the internal transport layer.
 *
 * \author Ramin Yaghoubzadeh Torky (ryaghoubzadeh@uni-bielefeld.de)
 * \date   December, 2018
 */

#ifndef __ipaaca_config_h_INCLUDED__
#define __ipaaca_config_h_INCLUDED__

#ifndef __ipaaca_h_INCLUDED__
#error "Please do not include this file directly, use ipaaca.h instead"
#endif

class Config {
    public:
        typedef std::shared_ptr<Config> ptr;
    protected:
        std::map<std::string, std::string> _data;
        std::set<std::string> _messages_delivered;
        template<typename T> T get_with_default_internal(const std::string& key, T const& default_value, bool warn);
        template<typename T> void config_key_not_found(const std::string& key, T const& default_value, bool warn)
        {
            if (!_messages_delivered.count(key)) {
                if (warn) { IPAACA_WARNING("Config: no key '" << key << "', using default value " << default_value) }
                else { IPAACA_DEBUG("Config: no key '" << key << "', using default value " << default_value) }
                _messages_delivered.insert(key);
            }
        }
        template<typename T> void config_conversion_failed(const std::string& key, T const& default_value)
        {
            if (!_messages_delivered.count(key)) {
                IPAACA_WARNING("Config: failed conversion for key '" << key << "', using default value " << default_value)
                _messages_delivered.insert(key);
            }
        }
        bool get_key_and_value(const std::string& line, std::string& key, std::string& value);
    public:
        inline std::map<std::string, std::string>::const_iterator data_cbegin() const { return _data.begin(); }
        inline std::map<std::string, std::string>::const_iterator data_cend() const { return _data.end(); }
        void populate_from_global_sources();
        void populate_from_environment();
        void populate_from_any_conf_files();
        void populate_from_conf_file(std::fstream& fs);
        template<typename T> T get_with_default(const std::string& key, T const& default_value) {
            return get_with_default_internal(key, default_value, false);
        }
        template<typename T> T get_with_default_and_warning(const std::string& key, T const& default_value) {
            return get_with_default_internal(key, default_value, true);
        }
        //inline std::map<std::string, std::string>::iterator begin() { return std::map<std::string, std::string>::begin(); }
        //inline std::map<std::string, std::string>::iterator end() { return std::map<std::string, std::string>::end(); }
};

Config::ptr get_global_config(bool auto_parse_on_demand=true);

#endif

