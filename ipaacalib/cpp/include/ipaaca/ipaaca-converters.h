/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca-internal.h
 *
 * \brief Header file for internal transport implementation (RSB).
 *
 * Users should not include this file directly, but use ipaaca.h
 *
 * \b Note: This file is only included during compilation of ipaaca,
 * for regular use, the full internal API is not exposed.
 * Users generally need never touch the internal transport layer.
 *
 * The file provides callback glue from RSB, and wire converters
 * for the respective ipaaca classes.
 *
 * \author Ramin Yaghoubzadeh (ryaghoubzadeh@uni-bielefeld.de)
 * \date   March, 2015
 */

#ifndef __ipaaca_converters_h_INCLUDED__
#define __ipaaca_converters_h_INCLUDED__

#ifndef __ipaaca_h_INCLUDED__
#error "Please do not include this file directly, use ipaaca.h instead"
#endif

namespace converters {

// Wrap a serialized inner object and a wire type in a protobuf::TransportLevelWrapper
std::string cooked_message(const std::string& raw_message, ipaaca::protobuf::TransportMessageType msg_type);

// protobuf serialization for all supported types (replaces converter repository)
std::string internal_serialize(std::shared_ptr<ipaaca::IU> iu);
//std::string internal_serialize(std::shared_ptr<ipaaca::Message> msg);
std::string internal_serialize(std::shared_ptr<ipaaca::IUPayloadUpdate> pup);
std::string internal_serialize(std::shared_ptr<ipaaca::IULinkUpdate> lup);
std::string internal_serialize(std::shared_ptr<protobuf::RemoteRequestResult> pb);
std::string internal_serialize(std::shared_ptr<protobuf::IURetraction> pb);
std::string internal_serialize(std::shared_ptr<protobuf::IUCommission> pb);
std::string internal_serialize(std::shared_ptr<protobuf::IUResendRequest> pb);
std::string internal_serialize(std::shared_ptr<protobuf::IUPayloadUpdateRequest> pb);
std::string internal_serialize(std::shared_ptr<protobuf::IULinkUpdateRequest> pb);
std::string internal_serialize(std::shared_ptr<protobuf::IUCommissionRequest> pb);

// deserialization (just switching here instead of the converter registry business)
std::shared_ptr<ipaaca::backend::Event> internal_deserialize(const std::string& wire);

class IUConverter {//{{{
	public:
		IPAACA_HEADER_EXPORT static std::string serialize(std::shared_ptr<ipaaca::IU> iu); // const rsb::AnnotatedData& data, std::string& wire);
		IPAACA_HEADER_EXPORT static std::shared_ptr<ipaaca::IUInterface> deserialize(const std::string& wire);
};//}}}
/*
IPAACA_HEADER_EXPORT class MessageConverter {//{{{
	public:
		IPAACA_HEADER_EXPORT static std::string serialize(std::shared_ptr<ipaaca::Message> msg); // const rsb::AnnotatedData& data, std::string& wire);
		IPAACA_HEADER_EXPORT static std::shared_ptr<ipaaca::RemoteMessage> deserialize(const std::string& wire);
};//}}}
*/
class IUPayloadUpdateConverter {//{{{
	public:
		IPAACA_HEADER_EXPORT static std::string serialize(std::shared_ptr<ipaaca::IUPayloadUpdate> pup); // const rsb::AnnotatedData& data, std::string& wire);
		IPAACA_HEADER_EXPORT static std::shared_ptr<ipaaca::IUPayloadUpdate> deserialize(const std::string& wire);
};//}}}
class IULinkUpdateConverter {//{{{
	public:
		IPAACA_HEADER_EXPORT static std::string serialize(std::shared_ptr<ipaaca::IULinkUpdate> lup); // const rsb::AnnotatedData& data, std::string& wire);
		IPAACA_HEADER_EXPORT static std::shared_ptr<ipaaca::IULinkUpdate> deserialize(const std::string& wire);
};//}}}

} // of namespace converters

#endif // __ipaaca_converters_h_INCLUDED__

