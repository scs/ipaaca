/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca-initializer.h
 *
 * \brief Header file for the global initializer unit
 *
 * Users should not include this file directly, but use ipaaca.h
 *
 * \author Ramin Yaghoubzadeh (ryaghoubzadeh@uni-bielefeld.de)
 * \date   February, 2019
 */

#ifndef __ipaaca_initializer_h_INCLUDED__
#define __ipaaca_initializer_h_INCLUDED__

#ifndef __ipaaca_h_INCLUDED__
#error "Please do not include this file directly, use ipaaca.h instead"
#endif

/** \brief Static library initialization for backend
 *
 * This static class (singleton) is called once (explicitly or on-demand).
 * Unless called manually, it is initialized when ipaaca is first used
 * (i.e. the first Buffer is created).
 */
class Initializer
{
	public:
		/// Initialize the backend [DEPRECATED] (old name, use initialize_backend() instead)
		[[deprecated("Use initialize_backend() instead")]]
		IPAACA_HEADER_EXPORT static void initialize_ipaaca_rsb_if_needed();
		/// Explicitly initialize the backend (usually not required). No effect if already initialized. Automatically called during first Buffer construction.
		IPAACA_HEADER_EXPORT static void initialize_backend();
		IPAACA_HEADER_EXPORT static bool initialized();
		IPAACA_HEADER_EXPORT static void dump_current_default_config();
	protected:
		/** Perform rsb pre-setup before the implicit initialization
		 * (when first instantiating something). Pre-setup includes
		 * finding the RSB plugin dir, looking through several parent
		 * directories for a path "deps/lib/rsb*"/plugins. The path
		 * can also be set directly (env var RSB_PLUGINS_CPP_PATH),
		 * which disables the automatic search.
		 */
		IPAACA_HEADER_EXPORT static void override_env_with_cmdline_set_variables();
		IPAACA_MEMBER_VAR_EXPORT static bool _initialized;
};

#endif

