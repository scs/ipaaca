/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca.h
 *
 * \brief Central header file for IPAACA-C++.
 *
 * This is the central header file for the C++ version of IPAACA. Users should <b>include this file only.</b>
 *
 * \author Ramin Yaghoubzadeh (ryaghoubzadeh@uni-bielefeld.de)
 * \date   March, 2015
 */

/**
\mainpage Documentation for IPAACA-C++
This is the automatically generated documentation for the C++ implementation of IPAACA.

Protocol version: $(IPAACA_PROTOCOL_VERSION_MAJOR).$(IPAACA_PROTOCOL_VERSION_MINOR) \n
Release number: $(IPAACA_CPP_RELEASE_NUMBER) \n
Release date: $(IPAACA_CPP_RELEASE_DATE)

<b>IPAACA, the Incremental Processing Architecture for Artificial Conversational Agents, is available via <a href="https://opensource.cit-ec.de/projects/ipaaca/wiki"><b>its project page on CITEC Open Source</b></a>.</b>

IPAACA is (c) 2009-2022 Social Cognitive Systems Group, CITEC, Bielefeld University, Germany

The corresponding maintainer of this C++ version is Ramin Yaghoubzadeh Torky, ryaghoubzadeh@uni-bielefeld.de

<h3>List of most relevant entry points for application developers:</h3>

Buffers: InputBuffer, OutputBuffer

IUs: IUInterface, IU, Message

IU handling (user-set): #IUEventHandlerFunction

IU payload contents: Payload, PayloadEntryProxy

<h3>Short examples:</h3>

<h4>Encapsulated listener</h4>
<pre>
#include <ipaaca/ipaaca.h>

class ReceiverCpp {
    public:
    void handle_iu_inbuf(ipaaca::IUInterface::ptr iu, ipaaca::IUEventType etype, bool local)
    {
        // Inspect category and IUEventType (ADDED, UPDATED...)
        std::cout << ipaaca::iu_event_type_to_str(etype) << " category=" << iu->category() << " uid=" << iu->uid() << std::endl;
        // Access payload
        for (auto kv : iu->payload()) {
            std::cout << "Payload entry '" << kv.first << "': " << ((std::string) kv.second) << std::endl;
        }
        // Access any links
        auto links = iu->get_all_links();
        for (auto kv : iu->get_all_links()) {
            std::cout << "'" << kv.first << "' links to these IUs: ";
            for (const auto& lnk : kv.second) {
                std::cout << lnk << "  ";
            }
            std::cout << std::endl;
        }
    }

    int run()
    {
        auto ib = ipaaca::InputBuffer::create("myRecvProgName", std::set<std::string>{"myCategory"});
        ib->register_handler(IPAACA_BIND_CLASS_HANDLER(&ReceiverCpp::handle_iu_inbuf, this)) // bind to member
        std::cout << "Listening for all IU events (until Ctrl-C) ..." << std::endl;
        while (true) {
            sleep(5);
        }
        return 0;
    }

};
int main(int argc, char** argv)
{
    ipaaca::__ipaaca_static_option_log_level = IPAACA_LOG_LEVEL_DEBUG; // if you want this
    ReceiverCpp rcpp;
    rcpp.run();
}
</pre>
<h4>Minimal sender</h4>
<pre>
#include <ipaaca/ipaaca.h>

int main(int argc, char** argv)
{
    auto out_buf = ipaaca::OutputBuffer::create("myProgramName");
    auto iu = ipaaca::Message::create("myCategory");
    iu->payload()["test"] = 123; // NOTE: see also #PayloadEntryProxy
    out_buf->add(iu);            // will be broadcast now
    return 0;
}
</pre>



*/

#ifndef __ipaaca_h_INCLUDED__
#define __ipaaca_h_INCLUDED__

/// ipaaca/IU/RSB protocol major version number
#define IPAACA_PROTOCOL_VERSION_MAJOR         4
/// ipaaca/IU/RSB protocol minor version number
#define IPAACA_PROTOCOL_VERSION_MINOR         1

/// running release number of ipaaca-c++
#define IPAACA_CPP_RELEASE_NUMBER             17
/// date of last release number increment
#define IPAACA_CPP_RELEASE_DATE     "2019-02-14"

#ifndef __FUNCTION_NAME__
	#if _WIN32 || _WIN64   // Windows
		#define __FUNCTION_NAME__   __FUNCTION__
	#else          // POSIX
		#define __FUNCTION_NAME__   __func__
	#endif
#endif

#if _WIN32 || _WIN64
	#define IPAACA_SYSTEM_DEPENDENT_CLASS_NAME(c) "class "##c
#else
	#define IPAACA_SYSTEM_DEPENDENT_CLASS_NAME(c) c
#endif

#if _WIN32 || _WIN64
	#if defined(IPAACA_STATIC_BINARY)
		#define IPAACA_EXPORT
		#define IPAACA_HEADER_EXPORT
		#define IPAACA_MEMBER_VAR_EXPORT
	#else
		#if defined(ipaaca_EXPORTS)
			#define  IPAACA_EXPORT
			#define  IPAACA_HEADER_EXPORT __declspec(dllexport)
			#define  IPAACA_MEMBER_VAR_EXPORT
		#else
			#define  IPAACA_EXPORT
			#define  IPAACA_HEADER_EXPORT __declspec(dllimport)
			#define  IPAACA_MEMBER_VAR_EXPORT
		#endif
	#endif
#else
	#define IPAACA_EXPORT
	#define IPAACA_HEADER_EXPORT
	#define IPAACA_MEMBER_VAR_EXPORT
#endif

// retrieve basename of source files for more readable logging
#define __FILE_BASENAME__ (strrchr(__FILE__,'/') ? strrchr(__FILE__,'/')+1 : __FILE__)
#if _WIN32 || _WIN64
	#define _IPAACA_LOG_COLOR_NORMAL ""
	#define _IPAACA_LOG_COLOR_DEBUG  ""
	#define _IPAACA_LOG_COLOR_WARN   ""
	#define _IPAACA_LOG_COLOR_ERROR  ""
#else
	#define _IPAACA_LOG_COLOR_NORMAL "\033[m"
	#define _IPAACA_LOG_COLOR_DEBUG "\033[37m"
	#define _IPAACA_LOG_COLOR_WARN "\033[38;5;208m"
	#define _IPAACA_LOG_COLOR_ERROR "\033[38;5;196m"
#endif
#ifdef IPAACA_DEBUG_MESSAGES
// Full debug messages mode
#define IPAACA_DEBUG(i)     if (__ipaaca_static_option_log_level>=IPAACA_LOG_LEVEL_DEBUG) { std::cout << _IPAACA_LOG_COLOR_DEBUG << "[Debug] " << __FILE_BASENAME__ << ":" << __LINE__ << ": " << __FUNCTION_NAME__ << "() -- " << i << _IPAACA_LOG_COLOR_NORMAL << std::endl; }
#define IPAACA_IMPLEMENT_ME if (__ipaaca_static_option_log_level>=IPAACA_LOG_LEVEL_INFO) { std::cout <<     "[Info]  " << __FILE_BASENAME__ << ":" << __LINE__ << ": " << __FUNCTION_NAME__ << "() -- IMPLEMENT ME" << std::endl; }
#define IPAACA_TODO(i)      if (__ipaaca_static_option_log_level>=IPAACA_LOG_LEVEL_INFO) { std::cout <<     "[Info]  " << __FILE_BASENAME__ << ":" << __LINE__ << ": " << __FUNCTION_NAME__ << "() -- TODO: " << i << std::endl; }
#else
#define IPAACA_DEBUG(i) ;
#define IPAACA_IMPLEMENT_ME ;
#define IPAACA_TODO(i) ;
#endif
// Always compile in info and above, even if debug messages disabled.
//  You can set __ipaaca_static_option_log_level (e.g. IPAACA_LOG_LEVEL_NONE for silence).
#define IPAACA_INFO(i)      if (__ipaaca_static_option_log_level>=IPAACA_LOG_LEVEL_INFO) { std::cout <<     "[Info]  " << __FILE_BASENAME__ << ":" << __LINE__ << ": " << __FUNCTION_NAME__ << "() -- " << i << std::endl; }
#define IPAACA_WARNING(i)   if (__ipaaca_static_option_log_level>=IPAACA_LOG_LEVEL_WARNING) { std::cout << _IPAACA_LOG_COLOR_WARN <<  "[WARN]  " << __FILE_BASENAME__ << ":" << __LINE__ << ": " << __FUNCTION_NAME__ << "() -- WARNING: " << i << _IPAACA_LOG_COLOR_NORMAL << std::endl; }
#define IPAACA_ERROR(i)     if (__ipaaca_static_option_log_level>=IPAACA_LOG_LEVEL_ERROR) { std::cout << _IPAACA_LOG_COLOR_ERROR <<    "[ERROR] " << __FILE_BASENAME__ << ":" << __LINE__ << ": " << __FUNCTION_NAME__ << "() -- ERROR: " << i << _IPAACA_LOG_COLOR_NORMAL << std::endl; }
#define IPAACA_CRITICAL(i)  if (__ipaaca_static_option_log_level>=IPAACA_LOG_LEVEL_CRITICAL) { std::cout << _IPAACA_LOG_COLOR_ERROR << "[CRITICAL] " << __FILE_BASENAME__ << ":" << __LINE__ << ": " << __FUNCTION_NAME__ << "() -- CRITICAL: " << i << _IPAACA_LOG_COLOR_NORMAL << std::endl; }

// new json-based payload API, used in several classes
#define RAPIDJSON_HAS_STDSTRING 1
#include "rapidjson/document.h"
#include "rapidjson/prettywriter.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/filewritestream.h"
#include <cstdio>

/// marking pure virtual functions for extra readability
#define _IPAACA_ABSTRACT_

#define _IPAACA_OVERRIDE_ override

/// value to return when reading nonexistant payload keys
#define IPAACA_PAYLOAD_DEFAULT_STRING_VALUE ""

// seconds until remote writes time out
#define IPAACA_REMOTE_SERVER_TIMEOUT 2.0


#include <iostream>
#include <fstream>

// for logger
#include <iomanip>

#if _WIN32 || _WIN64
// for Windows
//  time
#include <time.h>
//  and env
#define IPAACA_SETENV(k, v) _putenv_s(k, v);
#else
// for Linux and MacOS
//  time
#include <sys/time.h>
//  and env
#define IPAACA_SETENV(k, v) setenv(k, v, 1);
#endif

#include <cstdlib>


#if _WIN32 || _WIN64
#include <rpc.h>
#else
#include <uuid/uuid.h>
#include <glob.h>
#endif


#ifndef Q_MOC_RUN
#include <memory>
#include <functional>
#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>
#endif


#include <ipaaca/ipaaca.pb.h>

#include <set>
#include <list>
#include <algorithm>
#include <utility>
#include <initializer_list>

namespace ipaaca {

#include <ipaaca/ipaaca-definitions.h>
#include <ipaaca/ipaaca-forwards.h>
#include <ipaaca/ipaaca-errors.h>
#include <ipaaca/ipaaca-initializer.h>
#include <ipaaca/ipaaca-locking.h>
#include <ipaaca/ipaaca-cmdline-parser.h>
#include <ipaaca/ipaaca-string-utils.h>

// Global static library variables (run-time default configuration)
// Actual initial values are set in ipaaca.cc

/// Default payload type for new IUs (defaults to "JSON")
IPAACA_MEMBER_VAR_EXPORT extern std::string __ipaaca_static_option_default_payload_type;
/// Default channel for buffers (defaults to "default")
IPAACA_MEMBER_VAR_EXPORT extern std::string __ipaaca_static_option_default_channel;
/// Current console log level (defaults to warning), one of: IPAACA_LOG_LEVEL_CRITICAL, IPAACA_LOG_LEVEL_ERROR, IPAACA_LOG_LEVEL_WARNING, IPAACA_LOG_LEVEL_INFO, IPAACA_LOG_LEVEL_DEBUG, IPAACA_LOG_LEVEL_NONE
IPAACA_MEMBER_VAR_EXPORT extern unsigned int __ipaaca_static_option_log_level;

/// RSB host to connect to (defaults to "" = do not set, use global config)
IPAACA_MEMBER_VAR_EXPORT extern std::string __ipaaca_static_option_rsb_host;
/// RSB port to connect to (defaults to "" = do not set, use global config)
IPAACA_MEMBER_VAR_EXPORT extern std::string __ipaaca_static_option_rsb_port;
/// RSB transport to use (defaults to "" = do not set, use global config)
IPAACA_MEMBER_VAR_EXPORT extern std::string __ipaaca_static_option_rsb_transport;
/// Whether to run in server mode on 'socket' transport (defaults to "" = do not set, use global config)
IPAACA_MEMBER_VAR_EXPORT extern std::string __ipaaca_static_option_rsb_socketserver;

IPAACA_MEMBER_VAR_EXPORT Lock& logger_lock();

#if _WIN32 || _WIN64
#define LOG_IPAACA_CONSOLE(msg) { ipaaca::Locker logging_locker(ipaaca::logger_lock()); std::time_t result = std::time(NULL); std::cout << "[LOG] " << std::asctime(std::localtime(&result)) << " : " << msg << std::endl; }
#else
// use normal gettimeofday() on POSIX
#define LOG_IPAACA_CONSOLE(msg) { ipaaca::Locker logging_locker(ipaaca::logger_lock()); timeval logging_tim; gettimeofday(&logging_tim, NULL); double logging_t1=logging_tim.tv_sec+(logging_tim.tv_usec/1000000.0); std::cout << "[LOG] " << std::setprecision(15) << logging_t1 << " : " << msg << std::endl; }
#endif

#if _WIN32 || _WIN64
#define IPAACA_SIMPLE_TIMER_BEGIN(N) ;
#define IPAACA_SIMPLE_TIMER_END(N, NAME) LOG_IPAACA_CONSOLE(NAME << " - time elapsed: Windows - IMPLEMENT ME")
#else
/// use IPAACA_SIMPLE_TIMER_BEGIN(mysymbol) to start time profiling at a line in code
/// Several blocks can be defined in the same stack frame if different symbols are chosen
#define IPAACA_SIMPLE_TIMER_BEGIN(N) struct timeval _ipaaca_timer_tvstart_ ## N; \
	gettimeofday(&_ipaaca_timer_tvstart_ ## N, NULL);
/// use IPAACA_SIMPLE_TIMER_END(mysymbol, "message") to print time elapsed since correpsonding _BEGIN
#define IPAACA_SIMPLE_TIMER_END(N, NAME) struct timeval _ipaaca_timer_tvend_ ## N; \
	gettimeofday(&_ipaaca_timer_tvend_ ## N, NULL); \
	long _ipaaca_timer_usecs_ ## N = (_ipaaca_timer_tvend_ ## N.tv_sec*1000000 + _ipaaca_timer_tvend_ ## N.tv_usec) - (_ipaaca_timer_tvstart_ ## N.tv_sec*1000000 + _ipaaca_timer_tvstart_ ## N.tv_usec); \
	LOG_IPAACA_CONSOLE(NAME << " - ̨́us elapsed: " << _ipaaca_timer_usecs_ ## N)
#endif

#include <ipaaca/ipaaca-config.h>
#include <ipaaca/ipaaca-converters.h>
#include <ipaaca/ipaaca-backend.h>

#include <ipaaca/ipaaca-payload.h>
#include <ipaaca/ipaaca-buffers.h>
#include <ipaaca/ipaaca-ius.h>



} // of namespace ipaaca

#endif


