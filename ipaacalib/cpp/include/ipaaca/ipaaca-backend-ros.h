/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca-backend.h
 *
 * \brief Header file for abstract backend participant implementation
 *   (used in the core library and as a base to derive specific backends).
 *
 * Users should not include this file directly, but use ipaaca.h
 *
 * \b Note: This file is only included during compilation of ipaaca,
 * for regular use, the full internal API is not exposed.
 * Users generally need never touch the internal transport layer.
 *
 * \author Ramin Yaghoubzadeh Torky (ryaghoubzadeh@uni-bielefeld.de)
 * \date   January, 2019
 */

#ifndef __ipaaca_backend_ros_h_INCLUDED__
#define __ipaaca_backend_ros_h_INCLUDED__

#ifndef __ipaaca_h_INCLUDED__
#error "Please do not include this file directly, use ipaaca.h instead"
#endif

}
// Backend-specific include[s]
#include "ros/ros.h"
#include "std_msgs/String.h"
namespace ipaaca {

#define _ROS_REMOTE_SERVER_MAX_QUEUED_REQUESTS 0

namespace backend {
namespace ros {

//
// START of backend-specific implementation
//
// here: ROS

// helper to encapsulate a wait-until-live mechanism
//  you can take this for other backends and adopt the friend class name
class ParticipantCore {
    friend class ROSBackEnd;
    protected:
        IPAACA_MEMBER_VAR_EXPORT std::condition_variable _condvar;
        IPAACA_MEMBER_VAR_EXPORT std::mutex _condvar_mutex;
        IPAACA_MEMBER_VAR_EXPORT bool _running;
        IPAACA_MEMBER_VAR_EXPORT bool _live;
    protected:
        IPAACA_HEADER_EXPORT ParticipantCore();
        IPAACA_HEADER_EXPORT void signal_live();
        IPAACA_HEADER_EXPORT bool wait_live(long timeout_milliseconds = 15000);
};

class ROSParticipant: public ParticipantCore {
    public:
        typedef std::shared_ptr<ROSParticipant> ptr;
    protected:
        IPAACA_MEMBER_VAR_EXPORT std::string _scope;
        IPAACA_MEMBER_VAR_EXPORT std::string host;
        IPAACA_MEMBER_VAR_EXPORT int port;
        IPAACA_MEMBER_VAR_EXPORT int keepalive;
    protected:
        // a pointer; odd, but see the ROSBackEnd constructor & destructor
        ::ros::NodeHandle* _node_handle;
    public:
        IPAACA_HEADER_EXPORT ROSParticipant(const ROSParticipant& orig) = delete; // forbid copy-construction for backend
        IPAACA_HEADER_EXPORT inline virtual ~ROSParticipant() { }
        IPAACA_HEADER_EXPORT ROSParticipant(::ros::NodeHandle* node, const std::string& scope, Config::ptr config = nullptr);
};

class ROSInformer: public ROSParticipant, public Informer {
    public:
        typedef std::shared_ptr<ROSInformer> ptr;
    protected:
        ::ros::Publisher _ros_pub;
    public:
        IPAACA_HEADER_EXPORT ROSInformer(::ros::NodeHandle* node, const std::string& scope, Config::ptr config = nullptr);
        IPAACA_HEADER_EXPORT virtual bool internal_publish(const std::string& wire);
};

class ROSListener: public ROSParticipant, public Listener {
    public:
        typedef std::shared_ptr<ROSListener> ptr;
    protected:
        IPAACA_MEMBER_VAR_EXPORT ::ros::Subscriber _ros_sub;
    public:
        IPAACA_HEADER_EXPORT ROSListener(::ros::NodeHandle* node, const std::string& scope, InputBuffer* buffer_ptr, Config::ptr config = nullptr);
        IPAACA_HEADER_EXPORT virtual void on_message(const std_msgs::String::ConstPtr& msg);
};

class ROSLocalServer: public ROSParticipant, public LocalServer {
    public:
        typedef std::shared_ptr<ROSLocalServer> ptr;
    protected:
        IPAACA_MEMBER_VAR_EXPORT ::ros::Subscriber _ros_sub;
        IPAACA_MEMBER_VAR_EXPORT std::map<std::string, ::ros::Publisher> _ros_pubs;
    protected:
        IPAACA_HEADER_EXPORT void send_result_for_request(const std::string& request_endpoint, const std::string& request_uid, int64_t result);
        IPAACA_HEADER_EXPORT ::ros::Publisher get_publisher(const std::string& endpoint);
    public:
        IPAACA_HEADER_EXPORT ROSLocalServer(::ros::NodeHandle* node, const std::string& scope, ipaaca::OutputBuffer* buffer_ptr, Config::ptr config = nullptr);
        IPAACA_HEADER_EXPORT virtual void on_message(const std_msgs::String::ConstPtr& msg);
};

class ROSRemoteServer: public ROSParticipant, public RemoteServer {
    public:
        typedef std::shared_ptr<RemoteServer> ptr;
    protected:
        IPAACA_MEMBER_VAR_EXPORT std::map<std::string, PendingRequest::ptr> _pending_requests;
        IPAACA_MEMBER_VAR_EXPORT ipaaca::Lock _pending_requests_lock;
        IPAACA_MEMBER_VAR_EXPORT std::string _remote_end_scope; // this is the actual important scope,
            // ROSParticipant::_scope is repurposed here for receiving replies
        IPAACA_MEMBER_VAR_EXPORT std::string _name; // using this (unique) auto-generated name
        IPAACA_MEMBER_VAR_EXPORT ::ros::Subscriber _ros_sub;
        IPAACA_MEMBER_VAR_EXPORT ::ros::Publisher _ros_pub;
    public:
        IPAACA_HEADER_EXPORT ROSRemoteServer(::ros::NodeHandle* node, const std::string& scope, Config::ptr config = nullptr);
        IPAACA_HEADER_EXPORT int64_t request_remote_payload_update(std::shared_ptr<IUPayloadUpdate> update);
        IPAACA_HEADER_EXPORT int64_t request_remote_link_update(std::shared_ptr<IULinkUpdate> update);
        IPAACA_HEADER_EXPORT int64_t request_remote_commission(std::shared_ptr<protobuf::IUCommission> update);
        IPAACA_HEADER_EXPORT int64_t request_remote_resend_request(std::shared_ptr<protobuf::IUResendRequest> update);
        IPAACA_HEADER_EXPORT PendingRequest::ptr queue_pending_request(Event::ptr request);
        IPAACA_HEADER_EXPORT int64_t blocking_call(Event::ptr request);
        IPAACA_HEADER_EXPORT virtual void on_message(const std_msgs::String::ConstPtr& msg);
};


class ROSBackEnd: public BackEnd
{
    public:
        typedef std::shared_ptr<ROSBackEnd> ptr;
        friend class BackEndLibrary;
    protected:
        bool _need_init;
        char* _cfakename;
        ::ros::NodeHandle* _node_handle;
        ::ros::AsyncSpinner* _spinner;
    protected:
        IPAACA_HEADER_EXPORT ROSBackEnd();
    public:
        IPAACA_HEADER_EXPORT static BackEnd::ptr get();
        IPAACA_HEADER_EXPORT void teardown();
        IPAACA_HEADER_EXPORT void init_once();
        IPAACA_HEADER_EXPORT Informer::ptr createInformer(const std::string& scope);
        IPAACA_HEADER_EXPORT Listener::ptr createListener(const std::string& scope, InputBuffer* buf);
        IPAACA_HEADER_EXPORT LocalServer::ptr createLocalServer(const std::string& scope, OutputBuffer* buf);
        IPAACA_HEADER_EXPORT RemoteServer::ptr createRemoteServer(const std::string& scope);
        IPAACA_HEADER_EXPORT inline std::string make_valid_scope(const std::string& scope) override {
            // strip the leading slash for the ROS node name (removing this
            // extra rule would lead to a global (non-movable) namespace
            if (scope.length() && (scope[0] == '/')) return scope.substr(1);
            else return scope;
        }
};

} // of namespace ros
} // of namespace backend

#endif // of __ipaaca_backend_ros_h_INCLUDED__
