/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca-backend.h
 *
 * \brief Header file for abstract backend participant implementation
 *   (used in the core library and as a base to derive specific backends).
 *
 * Users should not include this file directly, but use ipaaca.h
 *
 * \b Note: This file is only included during compilation of ipaaca,
 * for regular use, the full internal API is not exposed.
 * Users generally need never touch the internal transport layer.
 *
 * \author Ramin Yaghoubzadeh Torky (ryaghoubzadeh@uni-bielefeld.de)
 * \date   December, 2018
 */

#ifndef __ipaaca_backend_h_INCLUDED__
#define __ipaaca_backend_h_INCLUDED__

#ifndef __ipaaca_h_INCLUDED__
#error "Please do not include this file directly, use ipaaca.h instead"
#endif

namespace backend {

#if _WIN32 || _WIN64
inline std::string generate_client_id()
{
    // '-' not allowed e.g. in ROS
    std::string uuid = ipaaca::generate_uuid_string().substr(0,8);
    return uuid;
}
#else
// get simplified basename of the process name from /proc
//  (we could also decide to route argv here instead)
inline std::string get_simplified_process_name()
{
    std::fstream fs;
    fs.open("/proc/self/cmdline", std::fstream::in);
    if (!fs.is_open()) {
        IPAACA_DEBUG("Could not open /proc/self/cmdline")
        return "ipaaca_cpp";
    }
    std::string line;
    std::vector<std::string> tokens;
    if (!std::getline(fs, line)) {
        IPAACA_DEBUG("Failed to read a line from /proc/self/cmdline")
        fs.close();
        return "ipaaca_cpp";
    }
    fs.close();
    int cnt = ipaaca::str_split_append(line, tokens, "\000");
    if (cnt < 1) {
        IPAACA_DEBUG("Failed to get process name from /proc/self/cmdline")
        return "ipaaca_cpp";
    }
    line = tokens[0];
    cnt = ipaaca::str_split_wipe(line, tokens, "/");
    if (cnt < 1) {
        IPAACA_DEBUG("Failed to parse process name in /proc/self/cmdline")
        return "ipaaca_cpp";
    }
    std::string procname;
    line = tokens[cnt-1];
    for (auto ch: line) {
        if ((ch>='a' && ch<='z') || (ch>='A' && ch<='Z') || (ch>='0' && ch<='9') || (ch=='_')) {
            procname += ch;
        } else if (ch=='-' || ch=='.' || ch==' ') {
            procname += '_';
        }
    }
    if (procname=="") {
        IPAACA_DEBUG("Only unacceptable characters in process name from /proc/self/cmdline")
        return "ipaaca_cpp";
    } else {
        return procname;
    }
}
inline std::string generate_client_id()
{
    //std::stringstream ss;
    //ss << "Process_" << getpid() << "_ClientID_" << ipaaca::generate_uuid_string();
    //return ss.str();
    std::string uuid = get_simplified_process_name() + "_" + ipaaca::generate_uuid_string().substr(0,8);
    return uuid;
}
#endif

/*
 * class Scope {
    public:
        std::string scope_string;
        Scope(const std::string& sc): scope_string(sc) {}
        Scope(const Scope& scope): scope_string(scope.scope_string) {}
        inline operator std::string() const { return scope_string; }
};
*/

// RSB-like wrapper for {wire type; void ref to deserialized obj}
//  to avoid modifying the event handling side.
class Event {
    public:
        typedef std::shared_ptr<Event> ptr;
    protected:
        std::string _type;
        std::shared_ptr<void> _data;
    public:
        inline Event(const std::string& type, std::shared_ptr<void> data): _type(type), _data(data) { }
        inline std::string getType() const { return _type; }
        inline std::shared_ptr<void> getData() const { return _data; }
};


class PendingRequest {
    /** Encapsulation of a pending remote request with
    a facility to keep the requesting thread locked
    until the reply or a timeout unlocks it. */
    public:
        typedef std::shared_ptr<PendingRequest> ptr;
    protected:
        Event::ptr _pending_request;
        std::condition_variable _condvar;
        std::mutex _condvar_mutex;
        // block var
    public:
        std::string _request_uid;
        int _result;
    public:
        inline PendingRequest(Event::ptr req) {
            std::string uuid = ipaaca::generate_uuid_string();
            _request_uid = uuid.substr(0,8);
            _pending_request = req;
            _result = -1;
        }
        /// reply_with_result is called by the backend when a remote request reply has been received (and it was a known tracked request)
        inline void reply_with_result(int result) {
            _result = result;
            _condvar.notify_one(); // wake up the waiting thread
        };
        /// wait_for_reply is called internally by the user thread that attempts to modify remote IUs, it waits until the remote end replies
        inline int wait_for_reply(long timeout_milliseconds = 30000) {
            std::unique_lock<std::mutex> lock(_condvar_mutex);
            // the remote end will set the result >=0 (from the other thread), which suffices to continue
            auto success = _condvar.wait_for(lock, std::chrono::milliseconds(timeout_milliseconds), [this]{return this->_result >= 0;});
            if (!success) {
                IPAACA_ERROR("Request timeout: we did not receive a reply for a remote server request")
                // TODO could throw instead
            }
            return _result;
        }
};

//
// Abstract interface
//

class Informer {
    public:
        typedef std::shared_ptr<Informer> ptr;
    protected:
        std::string _client_id;
    public:
        IPAACA_HEADER_EXPORT inline virtual ~Informer() { }
        template<typename Data> IPAACA_HEADER_EXPORT inline bool publish(Data d) { return internal_publish(ipaaca::converters::internal_serialize(d)); }
    public:
        //
        // You MUST override these functions in the backend versions:
        //
        IPAACA_HEADER_EXPORT virtual bool internal_publish(const std::string& wire) { IPAACA_ERROR("Attempt to invoke abstract version of Informer::internal_publish"); throw NotImplementedError(); }
};
/*
IPAACA_HEADER_EXPORT virtual bool publish(ipaaca::IU::ptr) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(ipaaca::Message::ptr) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(ipaaca::IUPayloadUpdate::ptr) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(ipaaca::IULinkUpdate::ptr) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(std::shared_ptr<protobuf::RemoteRequestResult>) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(std::shared_ptr<protobuf::IURetraction>) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(std::shared_ptr<protobuf::IUCommission>) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(std::shared_ptr<protobuf::IUResendRequest>) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(std::shared_ptr<protobuf::IUPayloadUpdateRequest>) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(std::shared_ptr<protobuf::IULinkUpdateRequest>) = 0;
IPAACA_HEADER_EXPORT virtual bool publish(std::shared_ptr<protobuf::IUCommissionRequest>
*/

class Listener {
    public:
        typedef std::shared_ptr<Listener> ptr;
    protected:
        ipaaca::InputBuffer* _buffer;
    protected:
        Listener(ipaaca::InputBuffer* buffer): _buffer(buffer) { }
    public:
        IPAACA_HEADER_EXPORT inline virtual ~Listener() { }
        //inline Listener(const Scope& scope, InputBuffer* buffer_ptr, Config::ptr config = nullptr)) {}
        void relay_received_event_to_buffer(Event::ptr event);
        void relay_received_event_to_buffer_threaded(Event::ptr event);
};

class LocalServer {
    public:
        typedef std::shared_ptr<LocalServer> ptr;
    protected:
        ipaaca::OutputBuffer* _buffer;
    protected:
        LocalServer(ipaaca::OutputBuffer* buffer): _buffer(buffer) { }
    public:
        //inline LocalServer(const Scope& scope, ipaaca::OutputBuffer* buffer_ptr, Config::ptr config = nullptr): _buffer(buffer_ptr);
        IPAACA_HEADER_EXPORT inline virtual ~LocalServer() { }
        //IPAACA_HEADER_EXPORT virtual int64_t attempt_to_apply_remote_payload_update(std::shared_ptr<IUPayloadUpdate> update) = 0;
        //IPAACA_HEADER_EXPORT virtual int64_t attempt_to_apply_remote_link_update(std::shared_ptr<IULinkUpdate> update) = 0;
        //IPAACA_HEADER_EXPORT virtual int64_t attempt_to_apply_remote_commission(std::shared_ptr<protobuf::IUCommission> update) = 0;
        //IPAACA_HEADER_EXPORT virtual int64_t attempt_to_apply_remote_resend_request(std::shared_ptr<protobuf::IUResendRequest> update) = 0;
        IPAACA_HEADER_EXPORT int64_t attempt_to_apply_remote_payload_update(std::shared_ptr<IUPayloadUpdate> update);
        IPAACA_HEADER_EXPORT int64_t attempt_to_apply_remote_link_update(std::shared_ptr<IULinkUpdate> update);
        IPAACA_HEADER_EXPORT int64_t attempt_to_apply_remote_commission(std::shared_ptr<protobuf::IUCommission> update);
        IPAACA_HEADER_EXPORT int64_t attempt_to_apply_remote_resend_request(std::shared_ptr<protobuf::IUResendRequest> update);

};

class RemoteServer {
    public:
        typedef std::shared_ptr<RemoteServer> ptr;
    public:
        //inline RemoteServer(const Scope& scope, Config::ptr config = nullptr) {
        //}
        IPAACA_HEADER_EXPORT inline virtual ~RemoteServer() { }
        IPAACA_HEADER_EXPORT virtual int64_t request_remote_payload_update(std::shared_ptr<IUPayloadUpdate> update) = 0;
        IPAACA_HEADER_EXPORT virtual int64_t request_remote_link_update(std::shared_ptr<IULinkUpdate> update) = 0;
        IPAACA_HEADER_EXPORT virtual int64_t request_remote_commission(std::shared_ptr<protobuf::IUCommission> update) = 0;
        IPAACA_HEADER_EXPORT virtual int64_t request_remote_resend_request(std::shared_ptr<protobuf::IUResendRequest> update) = 0;
};

class BackEnd {
    public:
        typedef std::shared_ptr<BackEnd> ptr;
    protected:
        std::string _backend_name;
    public:
        BackEnd(std::string backend_name): _backend_name(backend_name) { }
        inline std::string name() const { return _backend_name; }
        virtual ~BackEnd() {}
        virtual void teardown() = 0;
        virtual LocalServer::ptr createLocalServer(const std::string& scope, OutputBuffer* buf) = 0;
        virtual Informer::ptr createInformer(const std::string& scope) = 0;
        virtual RemoteServer::ptr createRemoteServer(const std::string& scope) = 0;
        virtual Listener::ptr createListener(const std::string& scope, InputBuffer* buf) = 0;
        virtual std::string make_valid_scope(const std::string& scope) = 0;
};

class BackEndLibrary {
    public:
        typedef std::shared_ptr<BackEndLibrary> ptr;
    protected:
        std::map<std::string, BackEnd::ptr> _backends;
    public:
        BackEndLibrary() { }
        inline static BackEndLibrary::ptr get() {
            static BackEndLibrary::ptr lib;
            if (!lib) {
                lib = std::make_shared<BackEndLibrary>();
            }
            return lib;
        }
        inline bool register_backend(BackEnd::ptr backend) {
            if (_backends.count(backend->name())) {
                IPAACA_ERROR("Not registering another BackEnd with already known name: " << backend->name())
                return false;
            } else {
                _backends[backend->name()] = backend;
                return true;
            }
        }
        inline BackEnd::ptr get_default_backend() {
            static BackEnd::ptr _singleton;
            if (_singleton) return _singleton;
            if (_backends.size() == 0) {
                IPAACA_ERROR("No backends are registered inside the library - cannot continue")
                throw BackEndNotFoundError();
            }
            std::string preferred_backend = get_global_config()->get_with_default<std::string>("backend", "");
            if (preferred_backend != "") {
                if (_backends.count(preferred_backend)) {
                    _singleton = _backends[preferred_backend];
                } else {
                    IPAACA_ERROR("Failed to initialize the selected BackEnd " << preferred_backend)
                    throw BackEndNotFoundError();
                }
            } else {
                // just return the first value // FIXME config or clean precedence rule
                for (auto kv: _backends) {
                    IPAACA_WARNING("No 'backend' config found, selecting BackEnd " << kv.second->name())
                    _singleton = kv.second;
                    break;
                }
            }
            if (_singleton) return _singleton;
            throw BackEndNotFoundError(); // should not be reached; silence warnings
        }
};

inline BackEnd::ptr get_default_backend() {
    return BackEndLibrary::get()->get_default_backend();
}


} // of namespace backend


#endif

