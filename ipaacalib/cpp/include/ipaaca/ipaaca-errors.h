/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca-errors.h
 *
 * \brief Header file for all errors and exceptions
 *
 * Users should not include this file directly, but use ipaaca.h
 *
 * \author Ramin Yaghoubzadeh (ryaghoubzadeh@uni-bielefeld.de)
 * \date   February, 2019
 */

#ifndef __ipaaca_errors_h_INCLUDED__
#define __ipaaca_errors_h_INCLUDED__

#ifndef __ipaaca_h_INCLUDED__
#error "Please do not include this file directly, use ipaaca.h instead"
#endif

/**
 * Exception with string description
 */
class Exception: public std::exception//{{{
{
	protected:
		IPAACA_MEMBER_VAR_EXPORT std::string _description;
	public:
		IPAACA_HEADER_EXPORT inline Exception(const std::string& description=""): _description(description) { }
		IPAACA_HEADER_EXPORT inline ~Exception() throw() { }
		IPAACA_HEADER_EXPORT const char* what() const throw() {
			return _description.c_str();
		}
};//}}}
class Abort: public std::exception//{{{
{
	protected:
		IPAACA_MEMBER_VAR_EXPORT std::string _description;
	public:
		IPAACA_HEADER_EXPORT inline Abort(const std::string& description=""): _description(description) { }
		IPAACA_HEADER_EXPORT inline ~Abort() throw() { }
		IPAACA_HEADER_EXPORT const char* what() const throw() {
			return _description.c_str();
		}
};//}}}

/// BackEnd failed conditions, e.g. limits
class BackEndBadConditionError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~BackEndBadConditionError() throw() { }
		IPAACA_HEADER_EXPORT inline BackEndBadConditionError() { //std::shared_ptr<BackEnd> iu) {
			_description = "BackEndBadConditionError";
		}
};//}}}
/// BackEnd [hard-]failed to connect a participant
class BackEndConnectionFailedError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~BackEndConnectionFailedError() throw() { }
		IPAACA_HEADER_EXPORT inline BackEndConnectionFailedError() { //std::shared_ptr<BackEnd> iu) {
			_description = "BackEndConnectionFailedError";
		}
};//}}}
/// BackEnd not found
class BackEndNotFoundError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~BackEndNotFoundError() throw() { }
		IPAACA_HEADER_EXPORT inline BackEndNotFoundError() { //std::shared_ptr<BackEnd> iu) {
			_description = "BackEndNotFoundError";
		}
};//}}}
/// Unknown wire type encountered
class UnhandledWireTypeError: public Exception//{{{
{
    public:
        IPAACA_HEADER_EXPORT inline ~UnhandledWireTypeError() throw() { }
        IPAACA_HEADER_EXPORT inline UnhandledWireTypeError(int wire_type=-1) {
            if (wire_type>0) {
                _description = "UnhandledWireTypeError (wire type encountered: " + std::to_string(wire_type) + ")";
            } else {
                _description = "UnhandledWireTypeError (value not provided)";
            }
        }
        };//}}}
/// IU was not found in a buffer
class IUNotFoundError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUNotFoundError() throw() { }
		IPAACA_HEADER_EXPORT inline IUNotFoundError() { //std::shared_ptr<IU> iu) {
			_description = "IUNotFoundError";
		}
};//}}}
/// IU was already published
class IUPublishedError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUPublishedError() throw() { }
		IPAACA_HEADER_EXPORT inline IUPublishedError() {
			_description = "IUPublishedError";
		}
};//}}}
/// IU had already been committed to
class IUCommittedError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUCommittedError() throw() { }
		IPAACA_HEADER_EXPORT inline IUCommittedError() {
			_description = "IUCommittedError";
		}
};//}}}
/// IU had already been retracted
class IURetractedError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IURetractedError() throw() { }
		IPAACA_HEADER_EXPORT inline IURetractedError() {
			_description = "IURetractedError";
		}
};//}}}
/// Remote IU update failed because it had been modified in the mean time
class IUUpdateFailedError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUUpdateFailedError() throw() { }
		IPAACA_HEADER_EXPORT inline IUUpdateFailedError() {
			_description = "IUUpdateFailedError";
		}
};//}}}
/// Requested resend of old IU due to malformed channel specification
class IUResendRequestFailedError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUResendRequestFailedError() throw() { }
		IPAACA_HEADER_EXPORT inline IUResendRequestFailedError() {
			_description = "IUResendRequestFailedError";
		}
};//}}}
/// Write operation failed because IU had been set read-only
class IUReadOnlyError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUReadOnlyError() throw() { }
		IPAACA_HEADER_EXPORT inline IUReadOnlyError() {
			_description = "IUReadOnlyError";
		}
};//}}}
/// Buffer::add() failed because the IU had been previously placed in another buffer
class IUAlreadyInABufferError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUAlreadyInABufferError() throw() { }
		IPAACA_HEADER_EXPORT inline IUAlreadyInABufferError() {
			_description = "IUAlreadyInABufferError";
		}
};//}}}
/// A request was made that is only valid for an already published IU
class IUUnpublishedError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUUnpublishedError() throw() { }
		IPAACA_HEADER_EXPORT inline IUUnpublishedError() {
			_description = "IUUnpublishedError";
		}
};//}}}
/// IU had already been allocated a UID
class IUAlreadyHasAnUIDError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUAlreadyHasAnUIDError() throw() { }
		IPAACA_HEADER_EXPORT inline IUAlreadyHasAnUIDError() {
			_description = "IUAlreadyHasAnUIDError";
		}
};//}}}
/// IU had already been allocated an owner name
class IUAlreadyHasAnOwnerNameError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~IUAlreadyHasAnOwnerNameError() throw() { }
		IPAACA_HEADER_EXPORT inline IUAlreadyHasAnOwnerNameError() {
			_description = "IUAlreadyHasAnOwnerNameError";
		}
};//}}}
/// UID generation failed (Windows only)
class UUIDGenerationError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~UUIDGenerationError() throw() { }
		IPAACA_HEADER_EXPORT inline UUIDGenerationError() {
			_description = "UUIDGenerationError";
		}
};//}}}
/// Not implemented (e.g. invalid request parameters via backend)
class NotImplementedError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~NotImplementedError() throw() { }
		IPAACA_HEADER_EXPORT inline NotImplementedError() {
			_description = "NotImplementedError";
		}
};//}}}
/// PayloadEntryProxy requested type conversion failed (including lenient interpretation)
class PayloadTypeConversionError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~PayloadTypeConversionError() throw() { }
		IPAACA_HEADER_EXPORT inline PayloadTypeConversionError() {
			_description = "PayloadTypeConversionError";
		}
};//}}}
/// PayloadEntryProxy was addressed as list when not a list or as map when not a map
class PayloadAddressingError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~PayloadAddressingError() throw() { }
		IPAACA_HEADER_EXPORT inline PayloadAddressingError() {
			_description = "PayloadAddressingError";
		}
};//}}}
/// Malformed json was received for a Payload
class JsonParsingError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~JsonParsingError() throw() { }
		IPAACA_HEADER_EXPORT inline JsonParsingError() {
			_description = "JsonParsingError";
		}
};//}}}
/// PayloadEntryProxy invalidated (unused)
class PayloadEntryProxyInvalidatedError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~PayloadEntryProxyInvalidatedError() throw() { }
		IPAACA_HEADER_EXPORT inline PayloadEntryProxyInvalidatedError() {
			_description = "PayloadEntryProxyInvalidatedError";
		}
};//}}}
/// Iterator over Payload entries was invalidated by an intermediate IU update operation
class PayloadIteratorInvalidError: public Exception//{{{
{
	public:
		IPAACA_HEADER_EXPORT inline ~PayloadIteratorInvalidError() throw() { }
		IPAACA_HEADER_EXPORT inline PayloadIteratorInvalidError() {
			_description = "PayloadIteratorInvalidError";
		}
};//}}}

#endif

