/*
 * This file is part of IPAACA, the
 *  "Incremental Processing Architecture
 *   for Artificial Conversational Agents".
 *
 * Copyright (c) 2009-2022 Social Cognitive Systems Group
 *                         (formerly the Sociable Agents Group)
 *                         CITEC, Bielefeld University
 *
 * http://opensource.cit-ec.de/projects/ipaaca/
 * http://purl.org/net/ipaaca
 *
 * This file may be licensed under the terms of of the
 * GNU Lesser General Public License Version 3 (the ``LGPL''),
 * or (at your option) any later version.
 *
 * Software distributed under the License is distributed
 * on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
 * express or implied. See the LGPL for the specific language
 * governing rights and limitations.
 *
 * You should have received a copy of the LGPL along with this
 * program. If not, go to http://www.gnu.org/licenses/lgpl.html
 * or write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * The development of this software was supported by the
 * Excellence Cluster EXC 277 Cognitive Interaction Technology.
 * The Excellence Cluster EXC 277 is a grant of the Deutsche
 * Forschungsgemeinschaft (DFG) in the context of the German
 * Excellence Initiative.
 */

/**
 * \file   ipaaca-cmdline_parser.h
 *
 * \brief Header file for basic command line parser
 *
 * Users should not include this file directly, but use ipaaca.h
 *
 * \author Ramin Yaghoubzadeh (ryaghoubzadeh@uni-bielefeld.de)
 * \date   February, 2019
 */

#ifndef __ipaaca_cmdline_parser_h_INCLUDED__
#define __ipaaca_cmdline_parser_h_INCLUDED__

#ifndef __ipaaca_h_INCLUDED__
#error "Please do not include this file directly, use ipaaca.h instead"
#endif

// in ipaaca-cmdline-parser.cc
// additional misc classes ( Command line options )//{{{
/** \brief Command line argument container for CommandLineParser
 *
 * Contains the results of argument parsing from CommandLineParser::parse()
 *
 * The parser is preconfigured to handle some standard options:
 *
 * Option                          | Function
 * --------------------------------|------------------------------------------------------------------------------
 * --help                          | Print list of available options
 * --verbose                       | Set verbose flag
 * --character-name <name>         | Set character name (legacy)
 * --component-name <name>         | Set component name (legacy)
 * --ipaaca-payload-type <type>    | Set default ipaaca payload type (default JSON, set STR for legacy protocol)
 * --ipaaca-default-channel <name> | Set default channel name (default 'default')
 * --ipaaca-enable-logging <level> | Set console log level, one of NONE, DEBUG, INFO, WARNING, ERROR, CRITICAL
 * --rsb-enable-logging <level>    | Set rsb (transport) log level
 *
 */
class CommandLineOptions {
	public:
		IPAACA_HEADER_EXPORT inline CommandLineOptions()
		{ }
		IPAACA_HEADER_EXPORT inline ~CommandLineOptions() {
		}
		IPAACA_MEMBER_VAR_EXPORT std::map<std::string, std::string> param_opts;
		IPAACA_MEMBER_VAR_EXPORT std::map<std::string, bool> param_set;
	public:
		IPAACA_HEADER_EXPORT void set_option(const std::string& name, bool expect, const char* optarg);
		/// Get the option argument or default value (if the option expected an argument)
		IPAACA_HEADER_EXPORT std::string get_param(const std::string& o);
		/// Check whether option has been set
		IPAACA_HEADER_EXPORT bool is_set(const std::string& o);
		IPAACA_HEADER_EXPORT void dump();
	public:
		typedef std::shared_ptr<CommandLineOptions> ptr;
};

/**
 * \brief Command line parser for ipaaca programs.
 *
 * The parser is preconfigured to handle some standard options:
 *
 * Option                          | Function
 * --------------------------------|------------------------------------------------------------------------------
 * --help                          | Print list of available options
 * --verbose                       | Set verbose flag
 * --character-name <name>         | Set character name (legacy)
 * --component-name <name>         | Set component name (legacy)
 * --ipaaca-payload-type <type>    | Set default ipaaca payload type (default JSON, set STR for legacy protocol)
 * --ipaaca-default-channel <name> | Set default channel name (default 'default')
 * --ipaaca-enable-logging <level> | Set console log level, one of NONE, DEBUG, INFO, WARNING, ERROR, CRITICAL
 * --rsb-enable-logging <level>    | Set rsb (transport) log level
 *
 */
class CommandLineParser {
	protected:
		IPAACA_MEMBER_VAR_EXPORT std::map<char, std::string> longopt; // letter->name
		IPAACA_MEMBER_VAR_EXPORT std::map<std::string, char> shortopt; // letter->name
		IPAACA_MEMBER_VAR_EXPORT std::map<std::string, bool> options; //  name / expect_param
		IPAACA_MEMBER_VAR_EXPORT std::map<std::string, std::string> defaults; // for opt params
		IPAACA_MEMBER_VAR_EXPORT std::map<std::string, int> set_flag; // for paramless opts
	protected:
		IPAACA_HEADER_EXPORT CommandLineParser();
		IPAACA_MEMBER_VAR_EXPORT bool library_options_handled;
		IPAACA_HEADER_EXPORT bool consume_library_option(const std::string& name, bool expect, const char* optarg);
		IPAACA_HEADER_EXPORT void ensure_defaults_in( CommandLineOptions::ptr clo );
	public:
		IPAACA_HEADER_EXPORT inline ~CommandLineParser() { }
		/// Create a new parser object reference.
		IPAACA_HEADER_EXPORT static inline std::shared_ptr<CommandLineParser> create() {
			return std::shared_ptr<CommandLineParser>(new CommandLineParser());
		}
		IPAACA_HEADER_EXPORT void initialize_parser_defaults();
		IPAACA_HEADER_EXPORT void dump_options();
		/** \brief Add a user-defined option
		 *
		 * \param optname      The long option name, e.g. verbose for --verbose
		 * \param shortn       The short option (or \0 for none)
		 * \param expect_param Whether an argument is expected for the option
		 * \param defaultv     The default string value (unused if expect_param is false)
		 */
		IPAACA_HEADER_EXPORT void add_option(const std::string& optname, char shortn, bool expect_param, const std::string& defaultv);
		/** \brief Parse argument list and return result.
		 *
		 * Parse argument list (e.g. from main()) with the parser, consuming the internal options.
		 * The remaining options are packaged into a CommandLineOptions object.
		 */
		IPAACA_HEADER_EXPORT CommandLineOptions::ptr parse(int argc, char* const* argv);
	public:
		typedef std::shared_ptr<CommandLineParser> ptr;
};
//}}}

#endif

