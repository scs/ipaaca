# -*- coding: utf-8 -*-

# This file is part of IPAACA, the
#  "Incremental Processing Architecture
#   for Artificial Conversational Agents".
#
# Copyright (c) 2009-2022 Social Cognitive Systems Group
#                         CITEC, Bielefeld University
#
# http://opensource.cit-ec.de/projects/ipaaca/
# http://purl.org/net/ipaaca
#
# This file may be licensed under the terms of of the
# GNU Lesser General Public License Version 3 (the ``LGPL''),
# or (at your option) any later version.
#
# Software distributed under the License is distributed
# on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
# express or implied. See the LGPL for the specific language
# governing rights and limitations.
#
# You should have received a copy of the LGPL along with this
# program. If not, go to http://www.gnu.org/licenses/lgpl.html
# or write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The development of this software was supported by the
# Excellence Cluster EXC 277 Cognitive Interaction Technology.
# The Excellence Cluster EXC 277 is a grant of the Deutsche
# Forschungsgemeinschaft (DFG) in the context of the German
# Excellence Initiative.

from __future__ import division, print_function

import collections

import ipaaca.ipaaca_pb2
import ipaaca.defaults
import ipaaca.exception
import ipaaca.iu
import ipaaca.misc
import ipaaca.converter
import ipaaca.backend
import ipaaca.config

import threading
try:
    import queue
except:
    import Queue as queue
import uuid
import os
import time

try:
    import paho.mqtt.client as mqtt
    MQTT_ENABLED = True
except:
    MQTT_ENABLED = False

if not MQTT_ENABLED:
    def create_backend():
        return None
else:
    def create_backend():
        return MQTTBackend(name='mqtt')
    
    LOGGER = ipaaca.misc.get_library_logger()

    _REMOTE_SERVER_MAX_QUEUED_REQUESTS = -1 # unlimited
    _REMOTE_LISTENER_MAX_QUEUED_EVENTS = 1024 # 'Full' exception if exceeded

    class EventWrapper(object):
        def __init__(self, data):
            self.data = data

    class PendingRequest(object):
        '''Encapsulation of a pending remote request with
        a facility to keep the requesting thread locked
        until the reply or a timeout unlocks it.'''
        def __init__(self, request):
            self._request = request
            self._event = threading.Event()
            self._reply = None
            self._request_uid = str(uuid.uuid4())[0:8]
        def wait_for_reply(self, timeout=30.0):
            wr = self._event.wait(timeout)
            return None if wr is False else self._reply
        def reply_with_result(self, reply):
            self._reply = reply
            self._event.set()
            
    class Informer(object):
        '''Informer interface, wrapping an outbound port to MQTT'''
        def __init__(self, scope, config=None):
            self._scope = scope
            self._running = False
            self._live = False
            self._live_event = threading.Event()
            self._handlers = []
            #
            self._client_id = '%s.%s_%s'%(self.__module__, self.__class__.__name__, str(uuid.uuid4())[0:8])
            self._client_id += '_' + scope
            self._mqtt_client = mqtt.Client(self._client_id)
            self._host = config.get_with_default('transport.mqtt.host', 'localhost', warn=True)
            self._port = int(config.get_with_default('transport.mqtt.port', 1883, warn=True))
            self._mqtt_client.on_connect = self.mqtt_callback_on_connect
            self._mqtt_client.on_disconnect = self.mqtt_callback_on_disconnect
            self._mqtt_client.on_message = self.mqtt_callback_on_message
            self._mqtt_client.on_subscribe = self.mqtt_callback_on_subscribe
            #self._mqtt_client.on_publish = self.mqtt_callback_on_publish
            self.run_in_background()
        def deactivate(self):
            pass
        def deactivate_internal(self):
            self._mqtt_client.disconnect()
            self._mqtt_client = None
        def run_in_background(self):
            if not self._running:
                self._running = True
                self._mqtt_client.loop_start()
                self._mqtt_client.connect(self._host, self._port)
        def mqtt_callback_on_connect(self, client, userdata, flags, rc):
            if rc > 0:
                LOGGER.warning('MQTT connect failed, result code ' + str(rc))
            else:
                self._live = True
                self._live_event.set()
        def mqtt_callback_on_subscribe(self, client, userdata, mid, granted_qos):
            # TODO should / could track how many / which topics have been granted
            if any(q != 2 for q in granted_qos):
                LOGGER.warning('MQTT subscription did not obtain QoS level 2')
        def mqtt_callback_on_disconnect(self, client, userdata, rc):
            LOGGER.warning('MQTT disconnect for '+str(self._scope)+' with result code '+str(rc))
        def mqtt_callback_on_message(self, client, userdata, message):
            pass
        def publishData(self, data):
            #print('Informer publishing '+str(data.__class__.__name__)+' on '+self._scope)
            self._mqtt_client.publish(self._scope, ipaaca.converter.serialize(data), qos=2)

    class BackgroundEventDispatcher(threading.Thread):
        def __init__(self, listener):
            super(BackgroundEventDispatcher, self).__init__()
            self.daemon = True
            self._listener = listener
        def terminate(self):
            self._running = False
        def run(self):
            self._running = True
            listener = self._listener
            while self._running: # auto-terminated (daemon)
                event = listener._event_queue.get(block=True, timeout=None)
                if event is None: return # signaled termination
                #print('\033[31mDispatch '+str(event.data.__class__.__name__)+' start ...\033[m')
                for handler in self._listener._handlers:
                    handler(event)
                #print('\033[32m... dispatch '+str(event.data.__class__.__name__)+' end.\033[m')

    class Listener(object):
        '''Listener interface, wrapping an inbound port from MQTT'''
        def __init__(self, scope, config=None):
            self._scope = scope
            self._running = False
            self._live = False
            self._live_event = threading.Event()
            self._handlers = []
            self._event_queue = queue.Queue(_REMOTE_LISTENER_MAX_QUEUED_EVENTS)
            #
            self._client_id = '%s.%s_%s'%(self.__module__, self.__class__.__name__, str(uuid.uuid4())[0:8])
            self._client_id += '_' + scope
            self._mqtt_client = mqtt.Client(self._client_id)
            self._host = config.get_with_default('transport.mqtt.host', 'localhost', warn=True)
            self._port = int(config.get_with_default('transport.mqtt.port', 1883, warn=True))
            self._mqtt_client.on_connect = self.mqtt_callback_on_connect
            self._mqtt_client.on_disconnect = self.mqtt_callback_on_disconnect
            self._mqtt_client.on_message = self.mqtt_callback_on_message
            self._mqtt_client.on_subscribe = self.mqtt_callback_on_subscribe
            #self._mqtt_client.on_socket_open = self.mqtt_callback_on_socket_open
            #self._mqtt_client.on_socket_close = self.mqtt_callback_on_socket_close
            #self._mqtt_client.on_log = self.mqtt_callback_on_log
            #self._mqtt_client.on_publish = self.mqtt_callback_on_publish
            self._dispatcher = BackgroundEventDispatcher(self)
            self._dispatcher.start()
            self.run_in_background()
        def deactivate(self):
            pass
        def deactivate_internal(self):
            self._event_queue.put(None, block=False) # signal termination, waking queue
            self._dispatcher.terminate()
            self._mqtt_client.disconnect()
            self._mqtt_client = None
        def run_in_background(self):
            if not self._running:
                self._running = True
                self._mqtt_client.loop_start()
                LOGGER.debug('Connect to '+str(self._host)+':'+str(self._port))
                self._mqtt_client.connect(self._host, self._port)
        #def mqtt_callback_on_log(self, client, userdata, level, buf):
        #    print('Listener: LOG: '+str(buf))
        def mqtt_callback_on_connect(self, client, userdata, flags, rc):
            if rc > 0:
                LOGGER.warning('MQTT connect failed, result code ' + str(rc))
            else:
                self._mqtt_client.subscribe(self._scope, qos=2)
        def mqtt_callback_on_subscribe(self, client, userdata, mid, granted_qos):
            # TODO should / could track how many / which topics have been granted
            if any(q != 2 for q in granted_qos):
                LOGGER.warning('MQTT subscription did not obtain QoS level 2')
            self._live = True
            self._live_event.set()
        def mqtt_callback_on_disconnect(self, client, userdata, rc):
            LOGGER.warning('MQTT disconnect for '+str(self._scope)+' with result code '+str(rc))
        def mqtt_callback_on_message(self, client, userdata, message):
            event = EventWrapper(ipaaca.converter.deserialize(message.payload))
            self._event_queue.put(event, block=False) # queue event for BackgroundEventDispatcher
        def addHandler(self, handler):
            self._handlers.append(handler)
        #def publishData(self, data):
        #    self._mqtt_client.publish(self._

    class LocalServer(object):
        '''LocalServer interface, allowing for RPC requests to
        IU functions, or reporting back success or failure.'''
        def __init__(self, buffer_impl, scope, config=None):
            self._buffer = buffer_impl
            self._scope = scope
            self._running = False
            self._live = False
            self._live_event = threading.Event()
            self._pending_requests_lock = threading.Lock()
            self._pending_requests = {}
            self._uuid = str(uuid.uuid4())[0:8]
            self._name = 'PID_' + str(os.getpid()) + '_LocalServer_' + self._uuid # unused atm
            #
            self._client_id = '%s.%s_%s'%(self.__module__, self.__class__.__name__, str(uuid.uuid4())[0:8])
            self._client_id += '_' + scope
            self._mqtt_client = mqtt.Client(self._client_id)
            self._host = config.get_with_default('transport.mqtt.host', 'localhost', warn=True)
            self._port = int(config.get_with_default('transport.mqtt.port', 1883, warn=True))
            self._mqtt_client.on_connect = self.mqtt_callback_on_connect
            self._mqtt_client.on_disconnect = self.mqtt_callback_on_disconnect
            self._mqtt_client.on_message = self.mqtt_callback_on_message
            self._mqtt_client.on_subscribe = self.mqtt_callback_on_subscribe
            #self._mqtt_client.on_publish = self.mqtt_callback_on_publish
            self.run_in_background()
        def deactivate(self):
            pass
        def deactivate_internal(self):
            self._mqtt_client.disconnect()
            self._mqtt_client = None
        def run_in_background(self):
            if not self._running:
                self._running = True
                self._mqtt_client.loop_start()
                self._mqtt_client.connect(self._host, self._port)
        def mqtt_callback_on_connect(self, client, userdata, flags, rc):
            if rc > 0:
                LOGGER.warning('MQTT connect failed, result code ' + str(rc))
            else:
                self._mqtt_client.subscribe(self._scope, qos=2)
        def mqtt_callback_on_subscribe(self, client, userdata, mid, granted_qos):
            # TODO should / could track how many / which topics have been granted
            if any(q != 2 for q in granted_qos):
                LOGGER.warning('MQTT subscription did not obtain QoS level 2')
            self._live = True
            self._live_event.set()
        def mqtt_callback_on_disconnect(self, client, userdata, rc):
            LOGGER.warning('MQTT disconnect for '+str(self._scope)+' with result code '+str(rc))
        def mqtt_callback_on_message(self, client, userdata, message):
            req = ipaaca.converter.deserialize(message.payload)
            result = None
            if isinstance(req, ipaaca.converter.IUPayloadUpdate):
                result = self.attempt_to_apply_remote_updatePayload(req)
            elif isinstance(req, ipaaca.converter.IULinkUpdate):
                result = self.attempt_to_apply_remote_updateLinks(req)
            elif isinstance(req, ipaaca.ipaaca_pb2.IUCommission):
                result = self.attempt_to_apply_remote_commit(req)
            elif isinstance(req, ipaaca.ipaaca_pb2.IUResendRequest):
                result = self.attempt_to_apply_remote_resendRequest(req)
            else:
                raise RuntimeError('LocalServer: got an object of wrong class '+str(req.__class__.__name__))  # TODO replace
            if result is not None:
                self.send_result_for_request(req, result)
        #
        def send_result_for_request(self, obj, result):
            pbo = ipaaca.ipaaca_pb2.RemoteRequestResult()
            pbo.result = result
            pbo.request_uid = obj.request_uid
            #print('Sending result to endpoint '+str(obj.request_endpoint))
            self._mqtt_client.publish(obj.request_endpoint, ipaaca.converter.serialize(pbo), qos=2)
        def attempt_to_apply_remote_updateLinks(self, obj):
            return self._buffer._remote_update_links(obj)
        def attempt_to_apply_remote_updatePayload(self, obj):
            return self._buffer._remote_update_payload(obj)
        def attempt_to_apply_remote_commit(self, obj):
            return self._buffer._remote_commit(obj)
        def attempt_to_apply_remote_resendRequest(self, obj):
            return self._buffer._remote_request_resend(obj)


    class RemoteServer(object):
        '''RemoteServer, connects to a LocalServer on the side
        of an actual IU owner, which will process any requests.
        The RemoteServer is put on hold while the owner is
        processing. RemoteServer is from RSB terminology,
        it might more aptly be described as an RPC client.'''
        def __init__(self, remote_end_scope, config=None):
            self._running = False
            self._live = False
            self._live_event = threading.Event()
            self._pending_requests_lock = threading.Lock()
            self._pending_requests = {}
            #queue.Queue(_REMOTE_SERVER_MAX_QUEUED_REQUESTS)
            self._uuid = str(uuid.uuid4())[0:8]
            self._name = 'PID_' + str(os.getpid()) + '_RemoteServer_' + self._uuid
            # will RECV here:
            self._scope = '/ipaaca/remotes/' + self._name
            # will SEND here
            self._remote_end_scope = remote_end_scope
            #
            self._client_id = '%s.%s_%s'%(self.__module__, self.__class__.__name__, str(uuid.uuid4())[0:8])
            self._client_id += '_' + remote_end_scope
            self._mqtt_client = mqtt.Client(self._client_id)
            self._host = config.get_with_default('transport.mqtt.host', 'localhost', warn=True)
            self._port = int(config.get_with_default('transport.mqtt.port', 1883, warn=True))
            self._mqtt_client.on_connect = self.mqtt_callback_on_connect
            self._mqtt_client.on_disconnect = self.mqtt_callback_on_disconnect
            self._mqtt_client.on_message = self.mqtt_callback_on_message
            self._mqtt_client.on_subscribe = self.mqtt_callback_on_subscribe
            #self._mqtt_client.on_publish = self.mqtt_callback_on_publish
            self.run_in_background()
        def deactivate(self):
            pass
        def deactivate_internal(self):
            self._mqtt_client.disconnect()
            self._mqtt_client = None
        def run_in_background(self):
            if not self._running:
                self._running = True
                self._mqtt_client.loop_start()
                self._mqtt_client.connect(self._host, self._port)
        def mqtt_callback_on_connect(self, client, userdata, flags, rc):
            if rc > 0:
                LOGGER.warning('MQTT connect failed, result code ' + str(rc))
            else:
                self._mqtt_client.subscribe(self._scope, qos=2)
        def mqtt_callback_on_subscribe(self, client, userdata, mid, granted_qos):
            # TODO should / could track how many / which topics have been granted
            if any(q != 2 for q in granted_qos):
                LOGGER.warning('MQTT subscription did not obtain QoS level 2')
            self._live = True
            self._live_event.set()
        def mqtt_callback_on_disconnect(self, client, userdata, rc):
            LOGGER.warning('MQTT disconnect for '+str(self._scope)+' with result code '+str(rc))
        def mqtt_callback_on_message(self, client, userdata, message):
            reply = ipaaca.converter.deserialize(message.payload)
            if isinstance(reply, ipaaca.ipaaca_pb2.RemoteRequestResult):
                uid = reply.request_uid
                pending_request = None
                with self._pending_requests_lock:
                    if uid in self._pending_requests:
                        pending_request = self._pending_requests[uid]
                        del self._pending_requests[uid]
                if pending_request is None:
                    raise RuntimeError('RemoteServer: got a reply for request uid that is not queued: '+str(uid))
                else:
                    # provide result to other thread and unblock it
                    pending_request.reply_with_result(reply)
            else:
                raise RuntimeError('RemoteServer: got an object of wrong class '+str(reply.__class__.__name__))  # TODO replace
        def queue_pending_request(self, request):
            pending_request = PendingRequest(request)
            with self._pending_requests_lock:
                if _REMOTE_SERVER_MAX_QUEUED_REQUESTS>0 and len(self._pending_requests) >= _REMOTE_SERVER_MAX_QUEUED_REQUESTS:
                    raise RuntimeError('RemoteServer: maximum number of pending requests exceeded')   # TODO replace?
                else:
                    self._pending_requests[pending_request._request_uid] = pending_request
                    return pending_request
        # impl
        def blocking_call(self, request):
            # Broker's queue will raise before sending anything if capacity is exceeded
            pending_request = self.queue_pending_request(request)
            # complete and send request
            request.request_uid = pending_request._request_uid
            request.request_endpoint = self._scope
            self._mqtt_client.publish(self._remote_end_scope, ipaaca.converter.serialize(request), qos=2)
            # wait for other end to return result
            reply = pending_request.wait_for_reply()
            if reply is None:
                LOGGER.warning('A request timed out!')
                return 0
            else:
                return reply.result  # the actual int result
        # glue that quacks like the RSB version
        def resendRequest(self, req):
            return self.blocking_call(req)
        def commit(self, req):
            return self.blocking_call(req)
        def updatePayload(self, req):
            return self.blocking_call(req)
        def updateLinks(self, req):
            return self.blocking_call(req)

    class MQTTBackend(object):
        
        def __init__(self, name='mqtt'):
            # back-end initialization code
            self._config = ipaaca.config.get_global_config()
            self._name = name
            self._participants = set()

        def _get_name(self):
            return self._name
        name = property(_get_name)

        def teardown(self):
            LOGGER.info('MQTT teardown: waiting 1 sec for final deliveries')
            time.sleep(1)
            for p in self._participants:
                p.deactivate_internal()

        def Scope(self, scope_str):
            '''Scope adapter (glue replacing rsb.Scope)'''
            return str(scope_str)

        def createLocalServer(self, buffer_impl, scope, config=None):
            LOGGER.debug('Creating a LocalServer on '+str(scope))
            s = LocalServer(buffer_impl, scope, self._config if config is None else config)
            self._participants.add(s)
            s._live_event.wait(30.0)
            return s
         
        def createRemoteServer(self, scope, config=None):
            LOGGER.debug('Creating a RemoteServer on '+str(scope))
            s = RemoteServer(scope, self._config if config is None else config)
            self._participants.add(s)
            s._live_event.wait(30.0)
            return s

        def createInformer(self, scope, config=None, dataType="ignored in this backend"):
            LOGGER.debug('Creating an Informer on '+str(scope))
            s = Informer(scope, self._config if config is None else config)
            self._participants.add(s)
            s._live_event.wait(30.0)
            return s

        def createListener(self, scope, config=None):
            LOGGER.debug('Creating a Listener on '+str(scope))
            s = Listener(scope, self._config if config is None else config)
            self._participants.add(s)
            s._live_event.wait(30.0)
            return s
            


