# -*- coding: utf-8 -*-

# This file is part of IPAACA, the
#  "Incremental Processing Architecture
#   for Artificial Conversational Agents".
#
# Copyright (c) 2009-2022 Social Cognitive Systems Group
#                         CITEC, Bielefeld University
#
# http://opensource.cit-ec.de/projects/ipaaca/
# http://purl.org/net/ipaaca
#
# This file may be licensed under the terms of of the
# GNU Lesser General Public License Version 3 (the ``LGPL''),
# or (at your option) any later version.
#
# Software distributed under the License is distributed
# on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
# express or implied. See the LGPL for the specific language
# governing rights and limitations.
#
# You should have received a copy of the LGPL along with this
# program. If not, go to http://www.gnu.org/licenses/lgpl.html
# or write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The development of this software was supported by the
# Excellence Cluster EXC 277 Cognitive Interaction Technology.
# The Excellence Cluster EXC 277 is a grant of the Deutsche
# Forschungsgemeinschaft (DFG) in the context of the German
# Excellence Initiative.

from __future__ import division, print_function

import ipaaca.defaults
import ipaaca.exception
import ipaaca.iu
import ipaaca.misc
import ipaaca.converter

import threading
import uuid
import os
import time

LOGGER = ipaaca.misc.get_library_logger()

__registered_backends = {}
__backend_registration_done = False

def register_backends():
    global __registered_backends
    global __backend_registration_done
    if not __backend_registration_done:
        __backend_registration_done = True
        LOGGER.debug('Registering available back-ends')
        # register available backends
        # mqtt
        import ipaaca.backend_mqtt
        be = ipaaca.backend_mqtt.create_backend()
        if be is not None:
            __registered_backends[be.name] = be
            LOGGER.debug('Back-end '+str(be.name)+' added')

        # ros
        import ipaaca.backend_ros
        be = ipaaca.backend_ros.create_backend()
        if be is not None:
            __registered_backends[be.name] = be
            LOGGER.debug('Back-end '+str(be.name)+' added')

def get_default_backend():
    # TODO selection mechanism / config
    if not __backend_registration_done:
        register_backends()
    if len(__registered_backends) == 0:
        raise RuntimeError('No back-ends could be initialized for ipaaca-python')
    cfg = ipaaca.config.get_global_config()
    preferred = cfg.get_with_default('backend', None)
    if preferred is None:
        k = list(__registered_backends.keys())[0]
        if len(__registered_backends) > 1:
            LOGGER.warning('No preferred ipaaca.backend set, returning one of several (probably the first in list)')
            print('Using randomly selected back-end {}!'.format(k))
    else:
        if preferred in __registered_backends:
            k = preferred
        else:
            raise ipaaca.exception.BackendInitializationError(preferred)
    LOGGER.info('Back-end is '+str(k))
    return __registered_backends[k]


