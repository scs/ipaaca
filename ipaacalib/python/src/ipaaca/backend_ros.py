# -*- coding: utf-8 -*-

# This file is part of IPAACA, the
#  "Incremental Processing Architecture
#   for Artificial Conversational Agents".
#
# Copyright (c) 2009-2022 Social Cognitive Systems Group
#                         CITEC, Bielefeld University
#
# http://opensource.cit-ec.de/projects/ipaaca/
# http://purl.org/net/ipaaca
#
# This file may be licensed under the terms of of the
# GNU Lesser General Public License Version 3 (the ``LGPL''),
# or (at your option) any later version.
#
# Software distributed under the License is distributed
# on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
# express or implied. See the LGPL for the specific language
# governing rights and limitations.
#
# You should have received a copy of the LGPL along with this
# program. If not, go to http://www.gnu.org/licenses/lgpl.html
# or write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The development of this software was supported by the
# Excellence Cluster EXC 277 Cognitive Interaction Technology.
# The Excellence Cluster EXC 277 is a grant of the Deutsche
# Forschungsgemeinschaft (DFG) in the context of the German
# Excellence Initiative.

from __future__ import division, print_function

import collections
import sys

import ipaaca.ipaaca_pb2
import ipaaca.defaults
import ipaaca.exception
import ipaaca.iu
import ipaaca.misc
import ipaaca.converter
import ipaaca.backend

import ipaaca.config as config

LOGGER = ipaaca.misc.get_library_logger()



ROS_ENABLED, __try_guessing = False, False
try:
    import rospy
    from std_msgs.msg import String
    import base64
    ROS_ENABLED = True
except:
    LOGGER.debug('rospy or deps not found, ROS backend disabled')
    ROS_ENABLED = False

if not ROS_ENABLED:
    def create_backend():
        return None
else:
    def create_backend():
        return ROSBackend(name='ros')

    import threading
    try:
        import queue
    except:
        import Queue as queue
    import uuid
    import os
    import time
    import sys

    class EventWrapper(object):
        def __init__(self, data):
            self.data = data

    class PendingRequest(object):
        '''Encapsulation of a pending remote request with
        a facility to keep the requesting thread locked
        until the reply or a timeout unlocks it.'''
        def __init__(self, request):
            self._request = request
            self._event = threading.Event()
            self._reply = None
            self._request_uid = str(uuid.uuid4())[0:8]
        def wait_for_reply(self, timeout=30.0):
            wr = self._event.wait(timeout)
            return None if wr is False else self._reply
        def reply_with_result(self, reply):
            self._reply = reply
            self._event.set()
            
    class Informer(object):
        '''Informer interface, wrapping an outbound port to ROS'''
        def __init__(self, scope, config=None):
            self._scope = scope
            self._running = False
            self._live = False
            self._live_event = threading.Event()
            self._handlers = []
            #
            self._client_id = '%s.%s_%s'%(self.__module__, self.__class__.__name__, str(uuid.uuid4())[0:8])
            self._client_id += '_' + scope
            self._ros_pub = rospy.Publisher(self._scope, String, queue_size=100, tcp_nodelay=True, latch=True)
            self._host = config.get_with_default('transport.mqtt.host', 'localhost', warn=True)
            self._port = config.get_with_default('transport.mqtt.port', 1883, warn=True)
        def deactivate(self):
            pass
            #self._ros_pub.unregister()
            #self._ros_pub = None
        def publishData(self, data):
            self._ros_pub.publish(ROSBackend.serialize(data))

    class BackgroundEventDispatcher(threading.Thread):
        def __init__(self, event, handlers):
            super(BackgroundEventDispatcher, self).__init__()
            self.daemon = True
            self._event = event
            self._handlers = handlers
        def run(self):
            for handler in self._handlers:
                handler(self._event)

    class Listener(object):
        '''Listener interface, wrapping an inbound port from ROS'''
        def __init__(self, scope, config=None):
            self._scope = scope
            self._running = False
            self._live = False
            self._live_event = threading.Event()
            self._handlers = []
            #
            self._client_id = '%s.%s_%s'%(self.__module__, self.__class__.__name__, str(uuid.uuid4())[0:8])
            self._client_id += '_' + scope
            self._ros_sub = rospy.Subscriber(self._scope, String, self.on_message, tcp_nodelay=True)
            self._host = config.get_with_default('transport.mqtt.host', 'localhost', warn=True)
            self._port = config.get_with_default('transport.mqtt.port', 1883, warn=True)
        def deactivate(self):
            pass
            #self._ros_sub.unregister()
            #self._ros_sub = None
        def on_message(self, message):
            event = EventWrapper(ROSBackend.deserialize(message.data))
            ## (1) with extra thread:
            #dispatcher = BackgroundEventDispatcher(event, self._handlers)
            #dispatcher.start()
            ## or (2) with no extra thread:
            for handler in self._handlers:
                handler(event)
        def addHandler(self, handler):
            self._handlers.append(handler)

    class LocalServer(object):
        '''LocalServer interface, allowing for RPC requests to
        IU functions, or reporting back success or failure.'''
        def __init__(self, buffer_impl, scope, config=None):
            self._buffer = buffer_impl
            self._scope = scope
            self._running = False
            self._live = False
            self._live_event = threading.Event()
            self._pending_requests_lock = threading.Lock()
            self._pending_requests = {}
            self._uuid = str(uuid.uuid4())[0:8]
            self._name = 'PID_' + str(os.getpid()) + '_LocalServer_' + self._uuid # unused atm
            #
            self._client_id = '%s.%s_%s'%(self.__module__, self.__class__.__name__, str(uuid.uuid4())[0:8])
            self._client_id += '_' + scope
            self._ros_pubs = {}
            self._ros_sub = rospy.Subscriber(self._scope, String, self.on_message, tcp_nodelay=True)
            self._host = config.get_with_default('transport.mqtt.host', 'localhost', warn=True)
            self._port = config.get_with_default('transport.mqtt.port', 1883, warn=True)
        def get_publisher(self, endpoint):
            if endpoint in self._ros_pubs:
                return self._ros_pubs[endpoint]
            else:
                p = rospy.Publisher(endpoint, String, queue_size=10, tcp_nodelay=True, latch=True)
                self._ros_pubs[endpoint] = p
                return p
        def deactivate(self):
            pass
            #self._ros_sub.unregister()
            #for v in self._ros_pubs.values():
            #    v.unregister()
            #self._ros_sub = None
            #self._ros_pubs = {}
        def on_message(self, message):
            req = ROSBackend.deserialize(message.data)
            result = None
            if isinstance(req, ipaaca.converter.IUPayloadUpdate):
                result = self.attempt_to_apply_remote_updatePayload(req)
            elif isinstance(req, ipaaca.converter.IULinkUpdate):
                result = self.attempt_to_apply_remote_updateLinks(req)
            elif isinstance(req, ipaaca.ipaaca_pb2.IUCommission):
                result = self.attempt_to_apply_remote_commit(req)
            elif isinstance(req, ipaaca.ipaaca_pb2.IUResendRequest):
                result = self.attempt_to_apply_remote_resendRequest(req)
            else:
                raise RuntimeError('LocalServer: got an object of wrong class '+str(req.__class__.__name__))  # TODO replace
            if result is not None:
                self.send_result_for_request(req, result)
        #
        def send_result_for_request(self, obj, result):
            pbo = ipaaca.ipaaca_pb2.RemoteRequestResult()
            pbo.result = result
            pbo.request_uid = obj.request_uid
            #print('Sending result to endpoint '+str(obj.request_endpoint))
            pub = self.get_publisher(obj.request_endpoint)
            pub.publish(ROSBackend.serialize(pbo))
        def attempt_to_apply_remote_updateLinks(self, obj):
            return self._buffer._remote_update_links(obj)
        def attempt_to_apply_remote_updatePayload(self, obj):
            return self._buffer._remote_update_payload(obj)
        def attempt_to_apply_remote_commit(self, obj):
            return self._buffer._remote_commit(obj)
        def attempt_to_apply_remote_resendRequest(self, obj):
            return self._buffer._remote_request_resend(obj)


    _REMOTE_SERVER_MAX_QUEUED_REQUESTS = -1 # unlimited

    class RemoteServer(object):
        '''RemoteServer, connects to a LocalServer on the side
        of an actual IU owner, which will process any requests.
        The RemoteServer is put on hold while the owner is
        processing. RemoteServer is from RSB terminology,
        it might more aptly be described as an RPC client.'''
        def __init__(self, remote_end_scope, config=None):
            self._running = False
            self._live = False
            self._live_event = threading.Event()
            self._pending_requests_lock = threading.Lock()
            self._pending_requests = {}
            #queue.Queue(_REMOTE_SERVER_MAX_QUEUED_REQUESTS)
            self._uuid = str(uuid.uuid4())[0:8]
            self._name = 'PID_' + str(os.getpid()) + '_RemoteServer_' + self._uuid
            # will RECV here:
            self._scope = '/ipaaca/remotes/' + self._name
            # will SEND here
            self._remote_end_scope = remote_end_scope
            #
            self._client_id = '%s.%s_%s'%(self.__module__, self.__class__.__name__, str(uuid.uuid4())[0:8])
            self._client_id += '_' + remote_end_scope
            self._ros_pub = rospy.Publisher(self._remote_end_scope, String, queue_size=10, tcp_nodelay=True, latch=True)
            self._ros_sub = rospy.Subscriber(self._scope, String, self.on_message, tcp_nodelay=True)
            self._host = config.get_with_default('transport.mqtt.host', 'localhost', warn=True)
            self._port = config.get_with_default('transport.mqtt.port', 1883, warn=True)
        def deactivate(self):
            pass
            #self._ros_sub.unregister()
            #self._ros_pub.unregister()
            #self._ros_sub = None
            #self._ros_pub = None
        def on_message(self, message):
            reply = ROSBackend.deserialize(message.data)
            if isinstance(reply, ipaaca.ipaaca_pb2.RemoteRequestResult):
                uid = reply.request_uid
                pending_request = None
                with self._pending_requests_lock:
                    if uid in self._pending_requests:
                        pending_request = self._pending_requests[uid]
                        del self._pending_requests[uid]
                if pending_request is None:
                    raise RuntimeError('RemoteServer: got a reply for request uid that is not queued: '+str(uid))
                else:
                    # provide result to other thread and unblock it
                    pending_request.reply_with_result(reply)
            else:
                raise RuntimeError('RemoteServer: got an object of wrong class '+str(reply.__class__.__name__))  # TODO replace
        def queue_pending_request(self, request):
            pending_request = PendingRequest(request)
            with self._pending_requests_lock:
                if _REMOTE_SERVER_MAX_QUEUED_REQUESTS>0 and len(self._pending_requests) >= _REMOTE_SERVER_MAX_QUEUED_REQUESTS:
                    raise RuntimeError('RemoteServer: maximum number of pending requests exceeded')   # TODO replace?
                else:
                    self._pending_requests[pending_request._request_uid] = pending_request
                    return pending_request
        # impl
        def blocking_call(self, request):
            # Broker's queue will raise before sending anything if capacity is exceeded
            pending_request = self.queue_pending_request(request)
            # complete and send request
            request.request_uid = pending_request._request_uid
            request.request_endpoint = self._scope
            self._ros_pub.publish(ROSBackend.serialize(request))
            # wait for other end to return result
            reply = pending_request.wait_for_reply()
            if reply is None:
                LOGGER.warning('A request timed out!')
                return 0
            else:
                return reply.result  # the actual int result
        # glue that quacks like the RSB version
        def resendRequest(self, req):
            return self.blocking_call(req)
        def commit(self, req):
            return self.blocking_call(req)
        def updatePayload(self, req):
            return self.blocking_call(req)
        def updateLinks(self, req):
            return self.blocking_call(req)

    class ROSBackend(object):
        
        def __init__(self, name='ros'):
            #import logging
            # back-end initialization code
            self._name = name
            self._need_init = True
            #logging.basicConfig(level=logging.DEBUG)
        
        def init_once(self):
            '''Actual back-end initialization is only done when it is used'''
            if self._need_init:
                self._need_init = False
                self._config = config.get_global_config()
                try:
                    # generate a ROS node prefix from the basename of argv[0]
                    clean_name = ''.join([c for c in sys.argv[0].rsplit('/',1)[-1].replace('.', '_').replace('-','_') if c.lower() in 'abcdefghijklmnoprqstuvwxzy0123456789_'])
                except:
                    clean_name = ''
                rospy.init_node('ipaaca_python' if len(clean_name)==0 else clean_name,
                        anonymous=True, disable_signals=True)

        def _get_name(self):
            return self._name
        name = property(_get_name)

        def teardown(self):
            LOGGER.info('ROS teardown: waiting 1 sec for final deliveries')
            time.sleep(1)
            rospy.signal_shutdown('Done')

        @staticmethod
        def serialize(obj):
            #print('object class: '+obj.__class__.__name__)
            bb = ipaaca.converter.serialize(obj)
            st = str(base64.b64encode(bb))
            #print('serialized: '+str(st))
            return st
        
        @staticmethod
        def deserialize(msg):
            #print('got serialized: '+str(msg))
            bb = base64.b64decode(msg)
            return ipaaca.converter.deserialize(bb)
        
        def Scope(self, scope_str):
            '''Scope adapter (glue replacing rsb.Scope)'''
            # ROS graph resources must not start with a slash
            return str(scope_str)[1:] if scope_str.startswith('/') else str(scope_str)

        def createLocalServer(self, buffer_impl, scope, config=None):
            self.init_once()
            LOGGER.debug('Creating a LocalServer on '+str(scope))
            LOGGER.debug('              from thread '+threading.current_thread().name)
            s = LocalServer(buffer_impl, scope, self._config if config is None else config)
            #s._live_event.wait(30.0)
            return s
         
        def createRemoteServer(self, scope, config=None):
            self.init_once()
            LOGGER.debug('Creating a RemoteServer on '+str(scope))
            LOGGER.debug('               from thread '+threading.current_thread().name)
            s = RemoteServer(scope, self._config if config is None else config)
            #s._live_event.wait(30.0)
            return s

        def createInformer(self, scope, config=None, dataType="ignored in this backend"):
            self.init_once()
            LOGGER.debug('Creating an Informer on '+str(scope))
            LOGGER.debug('            from thread '+threading.current_thread().name)
            s = Informer(scope, self._config if config is None else config)
            #s._live_event.wait(30.0)
            return s

        def createListener(self, scope, config=None):
            self.init_once()
            LOGGER.debug('Creating a Listener on '+str(scope))
            LOGGER.debug('           from thread '+threading.current_thread().name)
            s = Listener(scope, self._config if config is None else config)
            #s._live_event.wait(30.0)
            return s
            


