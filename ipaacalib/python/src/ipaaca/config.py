# -*- coding: utf-8 -*-

# This file is part of IPAACA, the
#  "Incremental Processing Architecture
#   for Artificial Conversational Agents".
#
# Copyright (c) 2009-2022 Social Cognitive Systems Group
#                         CITEC, Bielefeld University
#
# http://opensource.cit-ec.de/projects/ipaaca/
# http://purl.org/net/ipaaca
#
# This file may be licensed under the terms of of the
# GNU Lesser General Public License Version 3 (the ``LGPL''),
# or (at your option) any later version.
#
# Software distributed under the License is distributed
# on an ``AS IS'' basis, WITHOUT WARRANTY OF ANY KIND, either
# express or implied. See the LGPL for the specific language
# governing rights and limitations.
#
# You should have received a copy of the LGPL along with this
# program. If not, go to http://www.gnu.org/licenses/lgpl.html
# or write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# The development of this software was supported by the
# Excellence Cluster EXC 277 Cognitive Interaction Technology.
# The Excellence Cluster EXC 277 is a grant of the Deutsche
# Forschungsgemeinschaft (DFG) in the context of the German
# Excellence Initiative.

from __future__ import division, print_function

import ipaaca.defaults
import ipaaca.exception
import ipaaca.iu
import ipaaca.misc

import os
import re
try:
    import configparser
except:
    import ConfigParser as configparser


LOGGER = ipaaca.misc.get_library_logger()

__global_config = None

class Config(object):
    def __init__(self):
        self._store = {}

    def get_with_default(self, key, default_value, warn=False):
        if key in self._store:
            return self._store[key]
        else:
            notif = LOGGER.warning if warn else LOGGER.debug
            notif('Config key '+str(key)+' not found, returning default of '+str(default_value))
            return default_value
    
    def populate_from_global_sources(self):
        self._store = {}
        self.populate_from_any_conf_files()
        self.populate_from_environment()
        #self.populate_from_argv_overrides() # TODO IMPLEMENT_ME

    def populate_from_any_conf_files(self):
        globalconf = os.getenv('HOME', '')+'/.config/ipaaca.conf'
        for filename in ['ipaaca.conf', globalconf]:
            try:
                f = open(filename, 'r')
                c = configparser.ConfigParser()
                c.readfp(f)
                f.close()
                LOGGER.info('Including configuration from '+filename)
                for k,v in c.items('ipaaca'):
                    self._store[k] = v
                return
            except:
                pass
        LOGGER.info('Could not load ipaaca.conf either here or in ~/.config')

    def populate_from_environment(self):
        for k, v in os.environ.items():
            if k.startswith('IPAACA_'):
                if re.match(r'^[A-Za-z0-9_]*$', k) is None:
                    LOGGER.warning('Ignoring malformed environment key')
                else:
                    if len(v)>1023:
                        LOGGER.warning('Ignoring long environment value')
                    else:
                        # remove initial IPAACA_ and transform key to dotted lowercase
                        trans_key = k[7:].lower().replace('_', '.')
                        self._store[trans_key] = v
                        LOGGER.debug('Configured from environment: '+str(trans_key)+'="'+str(v)+'"')


def get_global_config():
    global __global_config
    if __global_config is None:
        __global_config = Config()
        __global_config.populate_from_global_sources()
    return __global_config

