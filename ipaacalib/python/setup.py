#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  9 14:12:05 2016

@author: jpoeppel
"""

from setuptools import setup

import os
import sys
import subprocess

from os import path as op 
from distutils.spawn import find_executable
from setuptools.command.build_py import build_py
from setuptools.command.bdist_egg import bdist_egg
from distutils.command.build import build
from distutils.command.sdist import sdist

class ProtoBuild(build_py):
    """
    This command automatically compiles all .proto files with `protoc` compiler
    and places generated files near them -- i.e. in the same directory.
    """

    def find_protoc(self):
        "Locates protoc executable"

        if 'PROTOC' in os.environ and os.path.exists(os.environ['PROTOC']):
            protoc = os.environ['PROTOC']
        else:
            protoc = find_executable('protoc')

        if protoc is None:
            sys.stderr.write('protoc not found. Is protobuf-compiler installed? \n'
                             'Alternatively, you can point the PROTOC environment variable at a local version.')
            sys.exit(1)

        return protoc

    def run(self):
        #TODO determine path automaticall
        packagedir = "../proto"
        print("running build proto")
        for protofile in filter(lambda x: x.endswith('.proto'), os.listdir(packagedir)):
            source = op.join(packagedir, protofile)
            output = source.replace('.proto', '_pb2.py')

            if (not op.exists(output) or (op.getmtime(source) > op.getmtime(output))):
                sys.stderr.write('Protobuf-compiling ' + source + '\n')
                subprocess.check_call([self.find_protoc(), "-I={}".format(packagedir),'--python_out=./src/ipaaca', source])

class BDist_egg(bdist_egg):
    '''
    Simple wrapper around the normal bdist_egg command to require
    protobuf build before normal build.

    .. codeauthor:: jwienke

    '''

    def run(self):
        self.run_command('build_proto')
        bdist_egg.run(self)


class Build(build):
    '''
    Simple wrapper around the normal build command to require protobuf build
    before normal build.

    .. codeauthor:: jwienke
    '''

    def run(self):
        self.run_command('build_proto')
        build.run(self)


class Sdist(sdist):
    '''
    Simple wrapper around the normal sdist command to require protobuf build
    before generating the source distribution..

    .. codeauthor:: jwienke
    '''

    def run(self):
        # fetch the protocol before building the source distribution so that
        # we have a cached version and each user can rebuild the protocol
        # with his own protobuf version
        self.run_command('build_proto')
        sdist.run(self)


version = "0.1.3" #TODO determine correct version! ideally from git, maybe do something similar to rsb/setup.py

        
setup(name="ipaaca",
      version=version,
      author="Hendrik Buschmeier, Ramin Yaghoubzadeh, Sören Klett",
      author_email="hbuschme@uni-bielefeld.de,ryaghoubzadeh@uni-bielefeld.de,sklett@techfak.uni-bielefeld.de",
      license='LGPLv3+',
      url='https://opensource.cit-ec.de/projects/ipaaca',
      install_requires=["paho-mqtt", "six", "protobuf"],
      packages=["ipaaca", "ipaaca.util"],
      package_dir={"ipaaca":"src/ipaaca"},  

# TODO Do we want to add ipaaca_pb2.py to the egg or as separate package?
#      data_files=[("./ipaaca", ["ipaaca_pb2.py"])],
                  
#      dependency_links=[
#          'http://www.spread.org/files/'
#          'SpreadModule-1.5spread4.tgz#egg=SpreadModule-1.5spread4'],

      cmdclass ={
                 "build_proto": ProtoBuild,
                 "sdist": Sdist,
                 "build": Build,
                 "bdist_egg":BDist_egg
                 }
        
      )
